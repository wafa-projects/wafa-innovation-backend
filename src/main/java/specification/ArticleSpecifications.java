package specification;


import java.time.LocalDateTime;

import org.springframework.data.jpa.domain.Specification;

import com.wafa.api.entity.Article;


public class ArticleSpecifications {
	private ArticleSpecifications() {
		// not called
	}

	public static Specification<Article> getByArticleID(Long id) {
		return (root, query, cb) -> {
			if (id == null) {
				return cb.conjunction();
			}
			return cb.equal(root.get("articleId"), id);
		};
	}

	public static Specification<Article> getByDescription(String description) {
		return (root, query, cb) -> {
			if (description == null || description.equals("")) {
				return cb.conjunction();
			}
			return cb.like(root.get("description"), "%" + description + "%");
		};
	}
	


	public static Specification<Article> getByVisble(Boolean isVisible) {
		return (root, query, cb) -> {
			if (isVisible == null ) {
				return cb.conjunction();
			}
			return cb.equal(root.get("isVisible"), isVisible);
		};
	}
	
	
	public static Specification<Article> getByDateDebut(LocalDateTime dateDebut) {
		return (root, query, cb) -> {
			if (dateDebut == null) {
				return cb.conjunction();
			}
			return cb.greaterThanOrEqualTo(root.get("dateArticle"), dateDebut);
		};
	}
	
	public static Specification<Article> getByDateFin(LocalDateTime dateFin) {
		return (root, query, cb) -> {
			if (dateFin == null) {
				return cb.conjunction();
			}
			return cb.lessThanOrEqualTo(root.get("dateArticle"), dateFin);
		};
	}



	public static Specification<Article> getByNameUser(String nom) {
		return (root, query, cb) -> {
			if (nom == null ||nom.equals("")) {
				return cb.conjunction();
			}
			return cb.like(root.get("utilisateur").get("nom"), "%" + nom + "%");
		};
	}
	
	public static Specification<Article> getByIDUser(Long id) {
		return (root, query, cb) -> {
			if (id == null) {
				return cb.conjunction();
			}
			return cb.equal(root.get("utilisateur").get("utilisateurId"), id);
		};
	}
	
	public static Specification<Article> getByEmailUser(String email) {
		return (root, query, cb) -> {
			if (email == null || email.equals("")) {
				return cb.conjunction();
			}
			return cb.like(root.get("utilisateur").get("email"), "%" +email+ "%");
		};
	}
	
	public static Specification<Article> getByDeleteUser(Boolean isDelete) {
		return (root, query, cb) -> {
			if (isDelete == null ) {
				return cb.conjunction();
			}
			return cb.equal(root.get("utilisateur").get("isDeleted"), isDelete);
		};
	}
}
