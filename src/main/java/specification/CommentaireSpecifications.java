package specification;


import java.time.LocalDateTime;

import org.springframework.data.jpa.domain.Specification;

import com.wafa.api.entity.Article;
import com.wafa.api.entity.Commentaire;


public class CommentaireSpecifications {
	private CommentaireSpecifications() {
		// not called
	}

	public static Specification<Commentaire> getByID(Long id) {
		return (root, query, cb) -> {
			if (id == null) {
				return cb.conjunction();
			}
			return cb.equal(root.get("commentaireId"), id);
		};
	}

	public static Specification<Commentaire> getByDescription(String description) {
		return (root, query, cb) -> {
			if (description == null || description.equals("")) {
				return cb.conjunction();
			}
			return cb.like(root.get("description"), "%" + description + "%");
		};
	}
	


	public static Specification<Commentaire> getByVisble(Boolean isVisible) {
		return (root, query, cb) -> {
			if (isVisible == null ) {
				return cb.conjunction();
			}
			return cb.equal(root.get("isVisible"), isVisible);
		};
	}
	
	
	public static Specification<Commentaire> getByDateDebut(LocalDateTime dateDebut) {
		return (root, query, cb) -> {
			if (dateDebut == null) {
				return cb.conjunction();
			}
			return cb.greaterThanOrEqualTo(root.get("dateCommentaire"), dateDebut);
		};
	}
	
	public static Specification<Commentaire> getByDateFin(LocalDateTime dateFin) {
		return (root, query, cb) -> {
			if (dateFin == null) {
				return cb.conjunction();
			}
			return cb.lessThanOrEqualTo(root.get("dateCommentaire"), dateFin);
		};
	}



	public static Specification<Commentaire> getByNameUser(String nom) {
		return (root, query, cb) -> {
			if (nom == null ||nom.equals("")) {
				return cb.conjunction();
			}
			return cb.like(root.get("utilisateur").get("nom"), "%" + nom + "%");
		};
	}
	
	public static Specification<Commentaire> getByIDUser(Long id) {
		return (root, query, cb) -> {
			if (id == null) {
				return cb.conjunction();
			}
			return cb.equal(root.get("utilisateur").get("utilisateurId"), id);
		};
	}
	
	public static Specification<Commentaire> getByIDArticle(Long id) {
		return (root, query, cb) -> {
			if (id == null) {
				return cb.conjunction();
			}
			return cb.equal(root.get("article").get("articleId"), id);
		};
	}
	
	public static Specification<Commentaire> getByDeleteUser(Boolean isDelete) {
		return (root, query, cb) -> {
			if (isDelete == null ) {
				return cb.conjunction();
			}
			return cb.equal(root.get("utilisateur").get("isDeleted"), isDelete);
		};
	}
}
