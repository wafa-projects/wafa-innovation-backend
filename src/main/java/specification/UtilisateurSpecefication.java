package specification;

import org.springframework.data.jpa.domain.Specification;

import com.wafa.api.entity.Utilisateur;



public class UtilisateurSpecefication {
	private UtilisateurSpecefication() {
		// not called
	}

	public static Specification<Utilisateur> getById(Long id) {
		return (root, query, cb) -> {
			if (id == null) {
				return cb.conjunction();
			}
			return cb.equal(root.get("utilisateurId"), id);
		};
	}


	public static Specification<Utilisateur> getByNom(String nom) {
		return (root, query, cb) -> {
			if (nom == null || nom.equals("")) {
				return cb.conjunction();
			}
			return cb.like(root.get("nom"), "%" + nom + "%");
		};
	}

	public static Specification<Utilisateur> getByStatutUser(String statut) {
		return (root, query, cb) -> {
			if (statut == null || statut.equals("")) {
				return cb.conjunction();
			}
			return cb.equal(root.get("statut"), statut);
		};
	}
	
	public static Specification<Utilisateur> getDeletedUser(Boolean isDeleted) {
		return (root, query, cb) -> {
			if (isDeleted == null) {
				return cb.conjunction();
			}
			return cb.equal(root.get("isDeleted"), isDeleted);
		};
	}

	public static Specification<Utilisateur> getByEmail(String email) {
		return (root, query, cb) -> {
			if (email == null || email.equals("")) {
				return cb.conjunction();
			}
			return cb.like(root.get("email"), "%" + email + "%");
		};
	}

	
	public static Specification<Utilisateur> getByMatricule(String matricule) {
		return (root, query, cb) -> {
			if (matricule == null || matricule.equals("")) {
				return cb.conjunction();
			}
			return cb.like(root.get("matricule"), "%" + matricule + "%");
		};
	}
	

}
