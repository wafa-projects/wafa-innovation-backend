package specification.filter;


import java.time.LocalDateTime;

import lombok.Data;

@Data
public class CommentaireFilter extends Filter {
	
	 private Long articleId;
	 private Long commentaireId;	
	 private String description;
	 private Boolean isVisible=false;
	 private LocalDateTime dateDebut;
	 private LocalDateTime dateFin;
	 private String nomUser;
	 private Long idUser;
	 private Boolean isDeleteUser=false;
}
