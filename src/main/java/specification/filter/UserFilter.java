package specification.filter;


import lombok.Data;

@Data
public class UserFilter extends Filter {
	
	 private Long utilisateurId;	
	 private String nom;
	 private String email;
	 private String matricule;
	 private String statut;
	 private Boolean isDeleted=false;
}
