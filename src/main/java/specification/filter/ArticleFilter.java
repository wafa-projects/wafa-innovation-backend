package specification.filter;


import java.time.LocalDateTime;

import lombok.Data;

@Data
public class ArticleFilter extends Filter {
	
	 private Long articleId;	
	 private String description;
	 private Boolean isVisible=false;
	 private LocalDateTime dateDebut;
	 private LocalDateTime dateFin;
	 private String nomUser;
	 private Long idUser;
	 private String emailUser;
	 private Boolean isDeleteUser=false;
}
