package com.wafa.api;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableAsync;

import com.wafa.api.entity.AcceuilPage;
import com.wafa.api.repository.AcceuilPageRepository;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class, UserDetailsServiceAutoConfiguration.class })
@EnableAsync
public class WafaApplication extends SpringBootServletInitializer implements CommandLineRunner {

	@Autowired
	AcceuilPageRepository repository;
	public static void main(String[] args) {
		

		SpringApplication.run(WafaApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	System.out.println("Starting MyAppSampleWebappApplication (WAR)...");
	 return application.sources(WafaApplication.class).properties(loadproperties());
	
	}
	
	 private Properties loadproperties() {
         Properties props = new Properties();
         props.put("spring.config.location", "classpath:externe-conf/");
         return props;
      }
	
	@Override
	public void run(String... args) throws Exception {
		
		AcceuilPage acceuilPage=AcceuilPage.builder().description("La créativité est contagieuse").imageIsVisible(false)
				.phraseJours("La créativité est contagieuse").utilisateur(null).build();
		if(repository.findAll().size()==0){
			repository.save(acceuilPage);
		}
	}

}
