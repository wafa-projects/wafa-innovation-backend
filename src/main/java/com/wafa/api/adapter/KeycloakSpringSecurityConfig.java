//package com.wafa.api.adapter;
//
//import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
//import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.context.annotation.Bean;
//import org.springframework.security.web.authentication.session.NullAuthenticatedSessionStrategy;
//import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
//
//@KeycloakConfiguration
//@ConditionalOnProperty(name = "keycloak.enabled", havingValue = "true", matchIfMissing = true)
//public class KeycloakSpringSecurityConfig extends KeycloakWebSecurityConfigurerAdapter{
//
// 
//
//    /**
//     * Use NullAuthenticatedSessionStrategy for bearer-only tokens. Otherwise, use
//     * RegisterSessionAuthenticationStrategy.
//     */
//    @Bean
//    @Override
//    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
//        return new NullAuthenticatedSessionStrategy();
//    }
//
//
//}
