package com.wafa.api.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "CHOIX_REPONSE")
public class ChoixReponse {
	private static final String TEXT = "text";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_Choix")
	private Long choixId;

	@Column(name = "DESCRIPTION",columnDefinition = TEXT)
	@Lob
	private String description;

	@ManyToOne
	private Questionnaire questionnaire;
}
