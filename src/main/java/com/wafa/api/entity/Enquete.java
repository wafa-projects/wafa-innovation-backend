package com.wafa.api.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Enquete")
public class Enquete {
	private static final String TEXT = "text";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_Enquete")
	private Long enqueteId;

	@Column(name = "DESCRIPTION",columnDefinition = TEXT)
	@Lob
	private String description;
	
	@Column(name = "DATE_ENQUETE")
	private LocalDateTime dateEnquete = LocalDateTime.now();
		
	@Column(name = "titre")
	private String titre;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_enquete")
	private List<Questionnaire> questionnaires;
	
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_enquete")
	private List<Utilisateur> participants;
	
	@ManyToOne
	@JoinColumn(name = "ID_UTILISATEUR")
	private Utilisateur utilisateur;
}
