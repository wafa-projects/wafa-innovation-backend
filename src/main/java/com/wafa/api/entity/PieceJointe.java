package com.wafa.api.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "PIECE_JOINTE")
public class PieceJointe {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PIECE")
	private Long pieceId;

	@Column(name = "LIEN")
	private String lien;

	@Column(name = "TYPE")
	private String type;

	@Lob
	@Column(name = "FICHIER")
	private byte[] fichier;

}
