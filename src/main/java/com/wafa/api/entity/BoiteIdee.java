package com.wafa.api.entity;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "BOITE_IDEE")
public class BoiteIdee {
	private static final String TEXT = "text";
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_IDEE")
	private Long ideeId;

	@Column(name = "DESCRIPTION",columnDefinition = TEXT)
	@Lob
	private String description;
	
	@Column(name = "TITLE")
	@Lob
	private String title;
	
	@Column(name = "DATE_IDEE")
	private LocalDateTime dateIdee = LocalDateTime.now();
	
	@ManyToOne
	@JoinColumn(name = "ID_UTILISATEUR")
	private Utilisateur utilisateur;

	@OneToOne(cascade = CascadeType.ALL)
	private PieceJointe pieceJointe;
}
