package com.wafa.api.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ACCEUIL_PAGE")
public class AcceuilPage {
	private static final String TEXT = "text";
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ACCEUIL")
	private Long acceuilId;

	@Column(name = "DESCRIPTION",columnDefinition = TEXT)
	@Lob
	private String description;
	
	@Column(name = "IMAGE_LIEN")
	@Lob
	private String image;
	
	@ManyToOne
	@JoinColumn(name = "ID_UTILISATEUR")
	private Utilisateur utilisateur;
	
	@Column(name = "PHRASE_JOURS",columnDefinition = TEXT)
	@Lob
	private String phraseJours;
	
	@Column(name = "IMAGE_IS_VISIBLE")
	private Boolean imageIsVisible;
	

}
