package com.wafa.api.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "REPONSE_ENQUETE")
public class ReponseEnquete {
	private static final String TEXT = "text";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_REPONSE_ENQUETE")
	private Long reponseId;

//	@ManyToOne
//	@JoinColumn(name = "ID_CHOIX")
//	private ChoixReponse choixReponse;
	
	@ManyToOne
	@JoinColumn(name = "ID_QUESTIONNAIRE")
	private Questionnaire questionnaire;
	
	@Column(name = "REPONSE",columnDefinition = TEXT)
	@Lob
	private String reponse;
	
	@Column(name = "DATE_REPONSE")
	private LocalDateTime dateReponse = LocalDateTime.now(); 

	@ManyToOne
	@JoinColumn(name = "ID_UTILISATEUR")
	private Utilisateur utilisateur;
}
