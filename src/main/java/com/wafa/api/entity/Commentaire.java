package com.wafa.api.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "COMMENTAIRE")
public class Commentaire {
	private static final String TEXT = "text";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_COMMENTAIRE")
	private Long commentaireId;

	@Column(name = "DESCRIPTION", columnDefinition = TEXT)
	@Lob
	private String description;

	@Column(name = "DATE_COMMENTAIRE")
	private LocalDateTime dateCommentaire = LocalDateTime.now();

	@ManyToOne
	@JoinColumn(name = "ID_UTILISATEUR")
	private Utilisateur utilisateur;

	@Column(name = "IS_VISIBLE")
	private Boolean isVisible = false;

	@ManyToOne
	@JoinColumn(name = "ID_Article")
	private Article article;
}
