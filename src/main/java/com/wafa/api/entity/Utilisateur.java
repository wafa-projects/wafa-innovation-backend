package com.wafa.api.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "UTILISATEUR")
public class Utilisateur {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_UTILISATEUR")
	private Long utilisateurId;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "NOM")
	private String nom;

	@Column(name = "PRENOM")
	private String prenom;
	
	@Column(name = "PASSWORD")
	private String password;
	
	@Column(name = "TELEPHONE")
	private String telephone;
	
	@Column(name = "MATRICULE")
	private String matricule;
	
	@Column(name = "ENTITEE")
	private String entitee;
	
	@Column(name = "VILLE")
	private String ville;
	
	@Column(name = "REGION")
	private String region;
	
	@Column(name = "STATUT")
	private String statut;
	
	@Column(name = "ROLE")
	private String role;
	
	@Column(name = "IS_DELETED")
	private Boolean isDeleted=false;
	
	@ManyToMany
	private List<Enquete> enquetes;
	
	@Column(name = "PHOTO_PROFILE")
	@Lob
	private String photoProfile;

}
