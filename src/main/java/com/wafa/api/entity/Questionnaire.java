package com.wafa.api.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "QUESTIONNAIRE")
public class Questionnaire {
	private static final String TEXT = "text";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_QUESTION")
	private Long questionnaireId;

	@Column(name = "DESCRIPTION",columnDefinition = TEXT)
	@Lob
	private String description;

	@ManyToOne
	private Enquete enquete;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_questionnaire")
	private List<ChoixReponse> choixReponses;

}
