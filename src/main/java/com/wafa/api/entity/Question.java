package com.wafa.api.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "QUESTION")
public class Question {
	private static final String TEXT = "text";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_QUESTION")
	private Long questionId;

	@Column(name = "DESCRIPTION",columnDefinition = TEXT)
	@Lob
	private String description;

	
	@Column(name = "IS_VISIBLE")
	private Boolean isVisible;
	
	@Column(name = "DATE_QUESTION")
	private LocalDateTime dateQuestion = LocalDateTime.now();
	
	@Column(name = "DATE_PUBLICATION")
	private LocalDateTime datePublication;

	@ManyToOne
	@JoinColumn(name = "ID_UTILISATEUR")
	private Utilisateur utilisateur;

	@ManyToOne
	@JoinColumn(name = "ID_QUESTION_PARENT")
	private Question questionParent;

	public Question(Long questionId) {
		this.questionId = questionId;
	}
}
