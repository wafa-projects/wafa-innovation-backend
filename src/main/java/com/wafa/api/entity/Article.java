package com.wafa.api.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ARTICLE")
public class Article {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ARTICLE")
	private Long articleId;

	@Column(name = "DESCRIPTION")
	@Lob
	private String description;

	@Column(name = "ARTICLE")
	@Lob
	private String article;
	
	@Column(name = "DATE_ARTICLE")
	private LocalDateTime dateArticle = LocalDateTime.now();
	
	@Column(name = "DATE_PUBLICATION")
	private LocalDateTime datePublication;

	@Column(name = "IS_VISIBLE")
	private Boolean isVisible = false;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<Commentaire> commentaires;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<ActionLike> likes;

	@ManyToOne
	@JoinColumn(name = "ID_UTILISATEUR")
	private Utilisateur utilisateur;

	@OneToOne(cascade = CascadeType.ALL)
	private PieceJointe pieceJointe;
}
