package com.wafa.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wafa.api.dto.AcceuilDto;
import com.wafa.api.service.AcceuilPageService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping
@Validated
@CrossOrigin("*")
public class AcceuilController {

	@Autowired
	private AcceuilPageService service;
	
	 @PostMapping(value = "/admin/acceuilPage")
	  public ResponseEntity<AcceuilDto> saveUtilisateur(@RequestBody AcceuilDto acceuilPage) {
	    log.info("Creation d'acceuil");
	    return ResponseEntity.status(HttpStatus.CREATED)
	        .body(service.saveAcceuil(acceuilPage));
	  }
	 
	  @PutMapping(value = "/admin/acceuilPage")
	  public ResponseEntity<AcceuilDto> updateUtilisateur(@RequestBody AcceuilDto acceuilPage) {
	    log.info("Update acceuilPage");
	    return ResponseEntity.status(HttpStatus.NO_CONTENT)
	        .body(service.updateAcceuil(acceuilPage));
	  }
	  
	 @GetMapping(value = "/open/acceuilPage")
	  public ResponseEntity<AcceuilDto> getAcceuilPage() {
	    log.info("Récupération d'acceuilPage");
	    return ResponseEntity.status(HttpStatus.OK)
	        .body(service.getAcceuil());
	  }
	  
	
}
