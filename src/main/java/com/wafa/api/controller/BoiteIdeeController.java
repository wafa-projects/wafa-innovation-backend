package com.wafa.api.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wafa.api.dto.BoiteIdeeDto;
import com.wafa.api.service.BoiteIdeeExcel;
import com.wafa.api.service.BoiteIdeeService;
import com.wafa.api.util.Pagination;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping
@Validated
@CrossOrigin("*")
public class BoiteIdeeController {

	@Autowired
	private BoiteIdeeService service;

	@Autowired
	private BoiteIdeeExcel boiteIdeeExcel;

	@GetMapping(value = "/api/boiteIdee/{id}")
	public ResponseEntity<BoiteIdeeDto> getBoiteIdeeById(@PathVariable Long id) {
		log.info("Récupération d'commentaire par id");
		return ResponseEntity.status(HttpStatus.OK).body(service.getBoiteIdeeById(id));
	}

	@DeleteMapping(value = "/api/boiteIdee/{id}")
	public void deleteBoiteIdee(@PathVariable Long id) {
		log.info("delete BoiteIdee");
		service.deleteBoiteIdee(id);
	}

	@GetMapping(value = "/api/boiteIdee/user/{id}")
	public ResponseEntity<List<BoiteIdeeDto>> getBoiteIdeeByUserId(@PathVariable Long id) {
		log.info("Récupération d'commentaire par id");
		return ResponseEntity.status(HttpStatus.OK).body(service.getByUser(id));
	}

	@GetMapping(value = "/api/nbrIdee/{idUser}")
	public ResponseEntity<Long> nbrIdee(@PathVariable Long idUser) {
		log.info("Récupération de  demande par idArticle");
		return ResponseEntity.status(HttpStatus.OK).body(service.nbrIdee(idUser));
	}

	@GetMapping(value = "/api/boiteIdee")
	public ResponseEntity<Page<BoiteIdeeDto>> getBoiteIdeePagable(
			@RequestParam(value = "page", defaultValue = Pagination.DEFAULT_PAGE) int page,
			@RequestParam(value = "size", defaultValue = Pagination.DEFAULT_SIZE) int size) {
		log.info("Récupération des boiteIdees");
		return ResponseEntity.status(HttpStatus.OK).body(service.getBoiteIdeePagable(page, size));
	}

	@PostMapping(value = "/api/boiteIdee")
	public ResponseEntity<BoiteIdeeDto> saveBoiteIdee(@RequestBody BoiteIdeeDto boiteIdeeDto) {
		log.info("Creation d'un demandes");
		return ResponseEntity.status(HttpStatus.CREATED).body(service.saveBoiteIdee(boiteIdeeDto));
	}

	@PutMapping(value = "/api/boiteIdee/{id}")
	public ResponseEntity<BoiteIdeeDto> updateBoiteIdee(@RequestBody BoiteIdeeDto commentaireDto,
			@PathVariable Long id) {
		log.info("Update BoiteIdee");
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.updateBoiteIdee(commentaireDto, id));
	}

	@GetMapping(value = "/admin/boiteIdee/excel")
	public ResponseEntity<InputStreamResource> writeExcel() throws IOException {
		ByteArrayInputStream in = boiteIdeeExcel.writeExcel();
		HttpHeaders headers = new org.springframework.http.HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=boiteIdeeExcel.xlsx");
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
	}

}
