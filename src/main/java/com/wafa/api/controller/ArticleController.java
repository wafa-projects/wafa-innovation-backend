package com.wafa.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wafa.api.dto.ArticleDto;
import com.wafa.api.service.ArticleService;
import com.wafa.api.util.Pagination;

import lombok.extern.slf4j.Slf4j;
import specification.filter.ArticleFilter;

@Slf4j
@RestController
@RequestMapping
@Validated
@CrossOrigin("*")
public class ArticleController {

	@Autowired
	private ArticleService service;

	@PutMapping(value = "/open/article/valider/{id}")
	public ResponseEntity<ArticleDto> validereArticle(@PathVariable Long id) {
		log.info("valider article");
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.validereArticle(id));
	}

	@PostMapping(value = "/api/article/criteria")
	public ResponseEntity<Page<ArticleDto>> getDemandeByCriteria(@RequestBody ArticleFilter articleFilter) {
		log.info("Récupération de la liste des article");
		return ResponseEntity.status(HttpStatus.OK).body(service.getArticleByCriteria(articleFilter));
	}

	@GetMapping(value = "/api/article")
	public ResponseEntity<List<ArticleDto>> getAllArticle() {
		log.info("Récupération de la liste des Article");
		return ResponseEntity.status(HttpStatus.OK).body(service.getAllArticle());
	}

	@DeleteMapping(value = "/api/articles/{id}")
	public void deleteArticle(@PathVariable Long id) {
		log.info("delete article");
		service.deleteArticle(id);
	}

	@GetMapping(value = "/api/articles/{id}")
	public ResponseEntity<ArticleDto> getArticleById(@PathVariable Long id) {
		log.info("Récupération d'article par id");
		return ResponseEntity.status(HttpStatus.OK).body(service.getArticleById(id));
	}

	@PutMapping(value = "/open/articles/visible/{id}")
	public ResponseEntity<ArticleDto> visibleArticles(@PathVariable Long id) {
		log.info("Visible Question");
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.visibleArticles(id));
	}

	@GetMapping(value = "/api/articles")
	public ResponseEntity<Page<ArticleDto>> getArticlePagable(
			@RequestParam(value = "page", defaultValue = Pagination.DEFAULT_PAGE) int page,
			@RequestParam(value = "size", defaultValue = Pagination.DEFAULT_SIZE) int size) {
		log.info("Récupération de  demande par id");
		return ResponseEntity.status(HttpStatus.OK).body(service.getArticlePagable(page, size));
	}

	@PostMapping(value = "/api/articles")
	public ResponseEntity<ArticleDto> saveArticle(@RequestBody ArticleDto articleDto) {
		log.info("Creation d'un demandes");
		return ResponseEntity.status(HttpStatus.CREATED).body(service.saveArticle(articleDto));
	}

	@PutMapping(value = "/api/articles/{id}")
	public ResponseEntity<ArticleDto> updateArticle(@RequestBody ArticleDto articleDto, @PathVariable Long id) {
		log.info("Update Article");
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.updateArticle(articleDto, id));
	}

}
