package com.wafa.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wafa.api.dto.QuestionDto;
import com.wafa.api.service.QuestionService;
import com.wafa.api.util.Pagination;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@Validated
@CrossOrigin("*")
public class QuestionController {

	@Autowired
	private QuestionService service;

	@GetMapping(value = "/api/question/{id}")
	public ResponseEntity<QuestionDto> getQuestionById(@PathVariable Long id) {
		log.info("Récupération d'commentaire par id");
		return ResponseEntity.status(HttpStatus.OK).body(service.getQuestionById(id));
	}

	
	@DeleteMapping(value = "/api/question/{id}")
	  public void deleteQuestion(@PathVariable Long id) {
	    log.info("delete question");
	    service.deleteQuestion(id);
	  }
	@GetMapping(value = "/api/question/user/{idUser}")
	public ResponseEntity<Page<QuestionDto>> getQuestionPagable(
			@RequestParam(value = "page", defaultValue = Pagination.DEFAULT_PAGE) int page,
			@RequestParam(value = "size", defaultValue = Pagination.DEFAULT_SIZE) int size,
			@PathVariable Long idUser) {
		log.info("Récupération de  demande par idArticle");
		return ResponseEntity.status(HttpStatus.OK).body(service.getByUserPage(page, size, idUser));
	}
	
	@GetMapping(value = "/api/nbrQuestion/{idUser}")
	public ResponseEntity<Long> nbrQuestion(@PathVariable Long idUser) {
		log.info("Récupération de  demande par id User");
		return ResponseEntity.status(HttpStatus.OK).body(service.nbrQuestion(idUser));
	}
	
	@GetMapping(value = "/api/question")
	public ResponseEntity<Page<QuestionDto>> getQuestionPagable(
			@RequestParam(value = "page", defaultValue = Pagination.DEFAULT_PAGE) int page,
			@RequestParam(value = "size", defaultValue = Pagination.DEFAULT_SIZE) int size) {
		log.info("Récupération des questions");
		return ResponseEntity.status(HttpStatus.OK).body(service.getQuestionPagable(page, size));
	}

	@PostMapping(value = "/api/question")
	public ResponseEntity<QuestionDto> saveQuestion(@RequestBody QuestionDto commentaireDto) {
		log.info("Creation d'un demandes");
		return ResponseEntity.status(HttpStatus.CREATED).body(service.saveQuestion(commentaireDto));
	}

	@PutMapping(value = "/api/question/{id}")
	public ResponseEntity<QuestionDto> updateQuestion(@RequestBody QuestionDto commentaireDto,
			@PathVariable Long id) {
		log.info("Update Question");
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.updateQuestion(commentaireDto, id));
	}
	
	@PutMapping(value = "/api/question/visible/{id}")
	public ResponseEntity<QuestionDto> visibleQuestion(@PathVariable Long id) {
		log.info("Visible Question");
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.visibleQuestion(id));
	}

}
