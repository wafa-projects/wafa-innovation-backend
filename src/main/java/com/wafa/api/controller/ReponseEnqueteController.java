package com.wafa.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wafa.api.dto.ReponseEnqueteDto;
import com.wafa.api.service.ReponseEnqueteService;
import com.wafa.api.util.Pagination;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping
@Validated
@CrossOrigin("*")
public class ReponseEnqueteController {

	@Autowired
	private ReponseEnqueteService service;

	@GetMapping(value = "/api/reponseEnquete/{id}")
	public ResponseEntity<ReponseEnqueteDto> getReponseById(@PathVariable Long id) {
		log.info("Récupération d'commentaire par id");
		return ResponseEntity.status(HttpStatus.OK).body(service.getReponseEnqueteById(id));
	}
	
	@DeleteMapping(value = "/api/reponseEnquete/{id}")
	  public void deleteReponseEnquete(@PathVariable Long id) {
	    log.info("delete reponse Enquete");
	    service.deleteReponseEnquete(id);
	  }
	
	@PostMapping(value = "/api/reponseEnquete/user/{id}")
	public ResponseEntity<List<ReponseEnqueteDto>> getReponseById(@RequestBody List<ReponseEnqueteDto> reponseDtos,@PathVariable Long id) {
		log.info("Récupération d'commentaire par id");
		return ResponseEntity.status(HttpStatus.OK).body(service.saveAllReponse(reponseDtos, id));
	}

	@GetMapping(value = "/api/reponseEnquetes/{idQuestion}")
	public ResponseEntity<Page<ReponseEnqueteDto>> getReponseByQestionPagable(
			@RequestParam(value = "page", defaultValue = Pagination.DEFAULT_PAGE) int page,
			@RequestParam(value = "size", defaultValue = Pagination.DEFAULT_SIZE) int size,
			@PathVariable Long idQuestion) {
		log.info("Récupération de  demande par question");
		return ResponseEntity.status(HttpStatus.OK).body(service.getReponseByQestionPagable(page, size, idQuestion));
	}
	
	

	@PostMapping(value = "/api/reponseEnquete")
	public ResponseEntity<ReponseEnqueteDto> saveReponse(@RequestBody ReponseEnqueteDto reponseEnqueteDto) {
		log.info("Creation d'un demandes");
		return ResponseEntity.status(HttpStatus.CREATED).body(service.saveReponseEnquete(reponseEnqueteDto));
	}

	@PutMapping(value = "/api/reponseEnquete/{id}")
	public ResponseEntity<ReponseEnqueteDto> updateReponse(@RequestBody ReponseEnqueteDto reponseEnqueteDto,
			@PathVariable Long id) {
		log.info("Update Reponse");
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.updateReponseEnquete(reponseEnqueteDto, id));
	}
	
	

}
