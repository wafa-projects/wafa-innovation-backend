package com.wafa.api.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wafa.api.dto.EnqueteDto;
import com.wafa.api.service.EnqueteExcel;
import com.wafa.api.service.EnqueteService;
import com.wafa.api.util.Pagination;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping
@Validated
@CrossOrigin("*")
public class EnqueteController {

	@Autowired
	private EnqueteService service;
	
	@Autowired
	private EnqueteExcel enqueteExcel;

	@GetMapping(value = "/api/enquete/{id}")
	public ResponseEntity<EnqueteDto> getEnqueteById(@PathVariable Long id) {
		log.info("Récupération d'commentaire par id");
		return ResponseEntity.status(HttpStatus.OK).body(service.getEnqueteById(id));
	}

	@DeleteMapping(value = "/api/enquete/{id}")
	  public void deleteEnquete(@PathVariable Long id) {
	    log.info("delete enquete");
	    service.deleteEnquete(id);
	  }
	
	@GetMapping(value = "/api/enquete/{id}/user/{idUser}")
	public ResponseEntity<EnqueteDto> getEnqueteOfUser(@PathVariable Long id,@PathVariable Long idUser) {
		log.info("Récupération d'commentaire par id");
		return ResponseEntity.status(HttpStatus.OK).body(service.getEnqueteOfUser(id, idUser));
	}
	
	
	@GetMapping(value = "/api/enquete")
	public ResponseEntity<Page<EnqueteDto>> getEnquetePagable(
			@RequestParam(value = "page", defaultValue = Pagination.DEFAULT_PAGE) int page,
			@RequestParam(value = "size", defaultValue = Pagination.DEFAULT_SIZE) int size) {
		log.info("Récupération des enquetes");
		return ResponseEntity.status(HttpStatus.OK).body(service.getEnquetePagable(page, size));
	}

	@PostMapping(value = "/admin/enquete")
	public ResponseEntity<EnqueteDto> saveEnquete(@RequestBody EnqueteDto enqueteDto) {
		log.info("Creation d'un demandes");
		return ResponseEntity.status(HttpStatus.CREATED).body(service.saveEnquete(enqueteDto));
	}
	
	@PostMapping(value = "/admin/enqueteAll")
	public ResponseEntity<EnqueteDto> saveEnqueteWithQuestionnaire(@RequestBody EnqueteDto enqueteDto) {
		log.info("Creation d'un demandes");
		return ResponseEntity.status(HttpStatus.CREATED).body(service.saveEnqueteWithQuestionnaire(enqueteDto));
	}

	@PutMapping(value = "/admin/enquete/{id}")
	public ResponseEntity<EnqueteDto> updateEnquete(@RequestBody EnqueteDto commentaireDto,
			@PathVariable Long id) {
		log.info("Update Enquete");
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.updateEnquete(commentaireDto, id));
	}
	
	 @GetMapping(value = "/admin/enquete/excel/{id}")
	  public ResponseEntity<InputStreamResource> writeExcel(@PathVariable Long id) throws IOException{
	    ByteArrayInputStream in = enqueteExcel.writeExcel(id);
	    HttpHeaders headers = new org.springframework.http.HttpHeaders();
	    headers.add("Content-Disposition", "attachment; filename=enqueteExcel.xlsx");
	    return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
	  }
	
	
}
