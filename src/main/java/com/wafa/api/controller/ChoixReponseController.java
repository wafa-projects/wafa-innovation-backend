package com.wafa.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wafa.api.service.ChoixReponseService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping
@Validated
@CrossOrigin("*")
public class ChoixReponseController {

	@Autowired
	private ChoixReponseService service;

	  
		@DeleteMapping(value = "/api/choixReponse/{id}")
		  public void deleteChoix(@PathVariable Long id) {
		    log.info("delete choix");
		    service.deleteChoix(id);
		  }

}
