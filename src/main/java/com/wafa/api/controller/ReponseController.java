package com.wafa.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wafa.api.dto.ReponseDto;
import com.wafa.api.service.ReponseService;
import com.wafa.api.util.Pagination;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping
@Validated
@CrossOrigin("*")
public class ReponseController {

	@Autowired
	private ReponseService service;

	@GetMapping(value = "/api/reponse/{id}")
	public ResponseEntity<ReponseDto> getReponseById(@PathVariable Long id) {
		log.info("Récupération d'commentaire par id");
		return ResponseEntity.status(HttpStatus.OK).body(service.getReponseById(id));
	}

	@DeleteMapping(value = "/api/reponse/{id}")
	  public void deleteReponse(@PathVariable Long id) {
	    log.info("delete reponse");
	    service.deleteReponse(id);
	  }
	
	@GetMapping(value = "/api/reponse/{idQuestion}")
	public ResponseEntity<Page<ReponseDto>> getReponseByQestionPagable(
			@RequestParam(value = "page", defaultValue = Pagination.DEFAULT_PAGE) int page,
			@RequestParam(value = "size", defaultValue = Pagination.DEFAULT_SIZE) int size,
			@PathVariable Long idQuestion) {
		log.info("Récupération de  demande par question");
		return ResponseEntity.status(HttpStatus.OK).body(service.getReponseByQestionPagable(page, size, idQuestion));
	}
	
	

	@PostMapping(value = "/api/reponse")
	public ResponseEntity<ReponseDto> saveReponse(@RequestBody ReponseDto commentaireDto) {
		log.info("Creation d'un demandes");
		return ResponseEntity.status(HttpStatus.CREATED).body(service.saveReponse(commentaireDto));
	}

	@PutMapping(value = "/api/reponse/{id}")
	public ResponseEntity<ReponseDto> updateReponse(@RequestBody ReponseDto commentaireDto,
			@PathVariable Long id) {
		log.info("Update Reponse");
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.updateReponse(commentaireDto, id));
	}
	
	

}
