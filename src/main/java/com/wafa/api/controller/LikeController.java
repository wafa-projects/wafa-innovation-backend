package com.wafa.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wafa.api.dto.LikeDto;
import com.wafa.api.service.LikeService;
import com.wafa.api.util.Pagination;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping
@Validated
@CrossOrigin("*")
public class LikeController {

	@Autowired
	private LikeService service;

	@GetMapping(value = "/api/likes/{id}")
	public ResponseEntity<LikeDto> getLikeById(@PathVariable Long id) {
		log.info("Récupération d'like par id");
		return ResponseEntity.status(HttpStatus.OK).body(service.getLikeById(id));
	}
	
	@GetMapping(value = "/api/likes")
	public ResponseEntity<List<LikeDto>> getLikes() {
		log.info("Récupération d'like par id");
		return ResponseEntity.status(HttpStatus.OK).body(service.getLikes());
	}

	@GetMapping(value = "/api/likes/article/{idArticle}")
	public ResponseEntity<Page<LikeDto>> getLikePagable(
			@RequestParam(value = "page", defaultValue = Pagination.DEFAULT_PAGE) int page,
			@RequestParam(value = "size", defaultValue = Pagination.DEFAULT_SIZE) int size,
			@PathVariable Long idArticle) {
		log.info("Récupération de  demande par idArticle");
		return ResponseEntity.status(HttpStatus.OK).body(service.getByArticlePagable(page, size, idArticle));
	}

	@GetMapping(value = "/api/nbrLikes/{idArticle}")
	public ResponseEntity<Long> nbrLikes(@PathVariable Long idArticle) {
		log.info("Récupération de  demande par idArticle");
		return ResponseEntity.status(HttpStatus.OK).body(service.nbrLikes( idArticle));
	}
	
	@PostMapping(value = "/api/likes")
	public ResponseEntity<LikeDto> saveLike(@RequestBody LikeDto likeDto) {
		log.info("Creation d'un demandes");
		return ResponseEntity.status(HttpStatus.CREATED).body(service.saveLike(likeDto));
	}

	@PutMapping(value = "/api/likes/{id}")
	public ResponseEntity<LikeDto> updateLike(@RequestBody LikeDto likeDto,
			@PathVariable Long id) {
		log.info("Update Like");
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.updateLike(likeDto, id));
	}

}
