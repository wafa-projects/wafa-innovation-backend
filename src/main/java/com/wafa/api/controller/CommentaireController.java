package com.wafa.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wafa.api.dto.CommentaireDto;
import com.wafa.api.service.CommentaireService;
import com.wafa.api.util.Pagination;

import lombok.extern.slf4j.Slf4j;
import specification.filter.CommentaireFilter;

@Slf4j
@RestController
@RequestMapping
@Validated
@CrossOrigin("*")
public class CommentaireController {

	@Autowired
	private CommentaireService service;

	@GetMapping(value = "/api/commentaires/{id}")
	public ResponseEntity<CommentaireDto> getCommentaireById(@PathVariable Long id) {
		log.info("Récupération d'commentaire par id");
		return ResponseEntity.status(HttpStatus.OK).body(service.getCommentaireById(id));
	}
	
	@DeleteMapping(value = "/api/commentaires/{id}")
	  public void deleteCommentaire(@PathVariable Long id) {
	    log.info("Delete commentaire");
	    service.deleteCommentaire(id);;
	  }

	@PostMapping(value = "/api/commentaires/criteria")
	public ResponseEntity<Page<CommentaireDto>> getCommentaireByCriteria(@RequestBody CommentaireFilter commentaireFilter) {
		log.info("Récupération de la liste des article");
		return ResponseEntity.status(HttpStatus.OK).body(service.getCommentaireByCriteria(commentaireFilter));
	}
	@PutMapping(value = "/admin/commentaires/valider/{id}")
	public ResponseEntity<CommentaireDto> validereArticle(@PathVariable Long id) {
		log.info("valider commentaires");
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.validereCommentaire(id));
	}
	
	@GetMapping(value = "/api/commentaires/article/{idArticle}")
	public ResponseEntity<Page<CommentaireDto>> getCommentairePagable(
			@RequestParam(value = "page", defaultValue = Pagination.DEFAULT_PAGE) int page,
			@RequestParam(value = "size", defaultValue = Pagination.DEFAULT_SIZE) int size,
			@PathVariable Long idArticle) {
		log.info("Récupération de  demande par idArticle");
		return ResponseEntity.status(HttpStatus.OK).body(service.getByArticlePagable(page, size, idArticle));
	}

	@PostMapping(value = "/api/commentaires")
	public ResponseEntity<CommentaireDto> saveCommentaire(@RequestBody CommentaireDto commentaireDto) {
		log.info("Creation d'un demandes");
		return ResponseEntity.status(HttpStatus.CREATED).body(service.saveCommentaire(commentaireDto));
	}

	@PutMapping(value = "/api/commentaires/{id}")
	public ResponseEntity<CommentaireDto> updateCommentaire(@RequestBody CommentaireDto commentaireDto,
			@PathVariable Long id) {
		log.info("Update Commentaire");
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.updateCommentaire(commentaireDto, id));
	}

}
