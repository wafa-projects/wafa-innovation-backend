package com.wafa.api.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.wafa.api.dto.ValidationDTO;
import com.wafa.api.entity.PieceJointe;
import com.wafa.api.service.PieceJointeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping
@Validated
@CrossOrigin("*")
public class PieceJointeController {

	@Autowired
	private PieceJointeService service;

	@PostMapping(value = "/api/uploadFile/{id}")
	public ResponseEntity<ValidationDTO> uploadFichierPiece(@RequestParam("file") MultipartFile file,
			@PathVariable Long id) throws IOException {
		log.info("Upload d'une pieceJointe ");
//		if (file.getSize() > 60000000) {
//			throw new IllegalArgumentException("Le Fichier est trop volumineux");
//		}
		return ResponseEntity.status(HttpStatus.CREATED).body(service.uploadFichierPiece(file, id));
	}
	
	@GetMapping(value = "/open/filePiece/{id}")
	public ResponseEntity<byte[]> getFilePieceById(@PathVariable Long id) throws IOException {
		log.info("Upload d'une pieceJointe ");
//		if (file.getSize() > 60000000) {
//			throw new IllegalArgumentException("Le Fichier est trop volumineux");
//		}
		return ResponseEntity.status(HttpStatus.OK).body(service.getFilePieceById(id));
	}
	
	@GetMapping(value = "/open/download/{id}")
	public ResponseEntity<InputStreamResource> downloadPiece(@PathVariable Long id) throws IOException {
		ByteArrayInputStream in = service.downloadPieceById(id);
		PieceJointe jointe=service.getPieceJointeById(id);
		HttpHeaders headers = new org.springframework.http.HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=file."+jointe.getType());
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
	}
	
	@DeleteMapping(value = "/api/pieceJointe/{id}")
	  public void deletePieceJointe(@PathVariable Long id) {
	    log.info("delete piece Jointe");
	    service.deletePieceJointe(id);;
	  }

}
