package com.wafa.api.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.wafa.api.config.SecurityContextUtils;
import com.wafa.api.dto.UtilisateurDto;
import com.wafa.api.dto.ValidationDTO;
import com.wafa.api.service.UtilisateurService;
import com.wafa.api.util.Pagination;

import lombok.extern.slf4j.Slf4j;
import specification.filter.UserFilter;

@Slf4j
@RestController
@RequestMapping
@Validated
@CrossOrigin("*")
public class UtilisateurController {

	@Autowired
	private UtilisateurService service;

	


	@GetMapping(value = "/admin/utilisateurs")
	public ResponseEntity<List<UtilisateurDto>> getAllUtilisateur() {
		log.info("Récupération de la liste des demandes");
		String user=SecurityContextUtils.getUserName();
		return ResponseEntity.status(HttpStatus.OK).body(service.getAllUtilisateurNotDeleted());
	}

	@PostMapping(value = "/api/utilisateurs/criteria")
	public ResponseEntity<Page<UtilisateurDto>> getDemandeByCriteria(@RequestBody UserFilter userFilter) {
		log.info("Récupération de la liste des user");
		return ResponseEntity.status(HttpStatus.OK).body(service.getUserByCriteria(userFilter));
	}
	
	@PostMapping(value = "/open/utilisateurs/password")
	public ResponseEntity<Boolean> forgetPassword(@RequestBody UserFilter email) {
		log.info("Récupération de password user");
		return ResponseEntity.status(HttpStatus.OK).body(service.forgetPassword(email.getEmail()));
	}

	@PostMapping(value = "/api/utilisateurs/uploadPhoto/{id}")
	public ResponseEntity<ValidationDTO> uploadPhoto(@RequestParam("file") MultipartFile file, @PathVariable Long id)
			throws IOException {
		log.info("Upload d'une pieceJointe ");
		String user=SecurityContextUtils.getUserName(); 
		// if (file.getSize() > 60000000) {
		// throw new IllegalArgumentException("Le Fichier est trop volumineux");
		// }
		return ResponseEntity.status(HttpStatus.CREATED).body(service.uploadFichierPiece(file, id));
	}


	@GetMapping(value = "/api/nbrEnquete/{idUser}")
	public ResponseEntity<Long> countPartcipation(@PathVariable Long idUser) {
		log.info("Récupération de  nbr Enquetee");
		return ResponseEntity.status(HttpStatus.OK).body(service.countPartcipation(idUser));
	}
	
	@GetMapping(value = "/api/utilisateurs/token")
	public ResponseEntity<UtilisateurDto> getUtilisateurByToken() {
		log.info("Récupération d'utilisateur par id");
		String user=SecurityContextUtils.getUserName();
		
		return ResponseEntity.status(HttpStatus.OK).body(service.getUtilisateurByToken(user));
	}

	@GetMapping(value = "/api/utilisateurs/{idUser}")
	public ResponseEntity<UtilisateurDto> getUtilisateurById(@PathVariable Long idUser) {
		log.info("Récupération d'utilisateur par id");
		return ResponseEntity.status(HttpStatus.OK).body(service.getUtilisateurById(idUser));
	}

	@GetMapping(value = "/admin/utilisateur/")
	public ResponseEntity<Page<UtilisateurDto>> getUtilisateurPagable(
			@RequestParam(value = "page", defaultValue = Pagination.DEFAULT_PAGE) int page,
			@RequestParam(value = "size", defaultValue = Pagination.DEFAULT_SIZE) int size) {
		log.info("Récupération de  demande par id");
		return ResponseEntity.status(HttpStatus.OK).body(service.getUtilisateurPagable(page, size));
	}

	@PostMapping(value = "/open/utilisateurs")
	public ResponseEntity<UtilisateurDto> saveUtilisateur(@RequestBody UtilisateurDto utilisateurDto) {
		log.info("Creation d'un demandes");
		return ResponseEntity.status(HttpStatus.CREATED).body(service.saveUtilisateur(utilisateurDto));
	}

	@PostMapping(value = "/open/utilisateurs/auth")
	public ResponseEntity<UtilisateurDto> getUserByEmailAndPassword(@RequestBody UtilisateurDto utilisateurDto) {
		log.info("Creation d'un demandes");
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(service.getUserByEmailAndPassword(utilisateurDto.getEmail(), utilisateurDto.getPassword()));
	}

	@PutMapping(value = "/api/utilisateurs/{idUser}")
	public ResponseEntity<UtilisateurDto> updateUtilisateur(@RequestBody UtilisateurDto utilisateurDto,
			@PathVariable Long idUser) {
		log.info("Update Utilisateur");
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.updateUtilisateur(utilisateurDto, idUser));
	}

	@PutMapping(value = "/open/utilisateurs/valider/{idUser}")
	public ResponseEntity<UtilisateurDto> validereUtilisateur(@PathVariable Long idUser) {
		log.info("Update Utilisateur");
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.validereUtilisateur(idUser));
	}

	@PutMapping(value = "/admin/utilisateurs/delete/{id}")
	public ResponseEntity<UtilisateurDto> deleteLogicUser(@PathVariable Long id) {
		log.info("Update Utilisateur");
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.deleteLogicUser(id));
	}
	
	@GetMapping(value = "/open/utilisateurs/filePiece/{id}")
	public ResponseEntity<byte[]> getImageUserById(@PathVariable Long id) throws IOException {
		log.info("Download photo  ");
		String user=SecurityContextUtils.getUserName();
		return ResponseEntity.status(HttpStatus.OK).body(service.getImageUserById(id));
	}

}
