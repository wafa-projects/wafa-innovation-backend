package com.wafa.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.wafa.api.dto.UtilisateurDto;
import com.wafa.api.entity.Utilisateur;

@Mapper(config = MapStructConfig.class)
public interface UtilisateurMapper {

	Utilisateur mapToEntity(UtilisateurDto utilisateurDto);

	UtilisateurDto mapToDTO(Utilisateur utilisateur);

	List<Utilisateur> mapToListOfEntity(List<UtilisateurDto> utilisateurDtos);

	List<UtilisateurDto> mapToListOfDTO(List<Utilisateur> utilisateur);

}
