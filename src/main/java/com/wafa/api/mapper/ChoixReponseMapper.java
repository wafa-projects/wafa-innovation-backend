package com.wafa.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.wafa.api.dto.ChoixReponseDto;
import com.wafa.api.entity.ChoixReponse;

@Mapper(config = MapStructConfig.class, uses = { QuestionnaireMapper.class })
public interface ChoixReponseMapper {

	@Mapping(source = "choixReponseDto.questionnaireID", target = "questionnaire.questionnaireId")
	ChoixReponse mapToEntity(ChoixReponseDto choixReponseDto);

	@Mapping(source = "choixReponse.questionnaire.questionnaireId", target = "questionnaireID")
	ChoixReponseDto mapToDTO(ChoixReponse choixReponse);

	List<ChoixReponse> mapToListOfEntity(List<ChoixReponseDto> choixReponseDtos);

	List<ChoixReponseDto> mapToListOfDTO(List<ChoixReponse> choixReponses);

}
