package com.wafa.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.wafa.api.dto.LikeDto;
import com.wafa.api.entity.ActionLike;

@Mapper(config = MapStructConfig.class, uses = { UtilisateurMapper.class ,ArticleMapper.class })
public interface LikeMapper {
	
	@Mapping(source = "likeDto.articleId", target = "article.articleId")
	@Mapping(source = "likeDto.utilisateurId", target = "utilisateur.utilisateurId")
	ActionLike mapToEntity(LikeDto likeDto);

	@Mapping(source = "like.utilisateur.utilisateurId", target = "utilisateurId")
	@Mapping(source = "like.article.articleId", target = "articleId")
	LikeDto mapToDTO(ActionLike like);

	List<ActionLike> mapToListOfEntity(List<LikeDto> likeDtos);

	List<LikeDto> mapToListOfDTO(List<ActionLike> likes);

}
