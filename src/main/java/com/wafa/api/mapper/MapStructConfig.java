package com.wafa.api.mapper;

import org.mapstruct.MapperConfig;

@MapperConfig(uses = {DateMapper.class}, componentModel = "spring")
public interface MapStructConfig {
}
