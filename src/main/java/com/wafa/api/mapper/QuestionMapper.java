package com.wafa.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.wafa.api.dto.QuestionDto;
import com.wafa.api.entity.Question;

@Mapper(config = MapStructConfig.class, uses = { UtilisateurMapper.class })
public interface QuestionMapper {
	
	@Mapping(target = "questionParent", expression = "java(questionDto.getQuestionParentId() != null ? new Question(questionDto.getQuestionParentId()) : null)")
    @Mapping(source = "questionDto.utilisateurId", target = "utilisateur.utilisateurId")
	Question mapToEntity(QuestionDto questionDto);
	
	@Mapping(source = "question.questionParent.questionId", target = "questionParentId")
	@Mapping(source = "question.utilisateur.utilisateurId", target = "utilisateurId")
	QuestionDto mapToDTO(Question question);

	List<Question> mapToListOfEntity(List<QuestionDto> questionDtos);

	List<QuestionDto> mapToListOfDTO(List<Question> questions);

}
