package com.wafa.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.wafa.api.dto.CommentaireDto;
import com.wafa.api.entity.Commentaire;

@Mapper(config = MapStructConfig.class, uses = { UtilisateurMapper.class ,ArticleMapper.class })
public interface CommentaireMapper {
	
	@Mapping(source = "commentaireDto.articleId", target = "article.articleId")
	@Mapping(source = "commentaireDto.utilisateurId", target = "utilisateur.utilisateurId")
	Commentaire mapToEntity(CommentaireDto commentaireDto);

	@Mapping(source = "commentaire.utilisateur.utilisateurId", target = "utilisateurId")
	@Mapping(source = "commentaire.article.articleId", target = "articleId")
	CommentaireDto mapToDTO(Commentaire commentaire);

	List<Commentaire> mapToListOfEntity(List<CommentaireDto> commentaireDtos);

	List<CommentaireDto> mapToListOfDTO(List<Commentaire> commentaires);

}
