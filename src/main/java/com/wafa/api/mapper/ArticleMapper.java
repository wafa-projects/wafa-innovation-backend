package com.wafa.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.wafa.api.dto.ArticleDto;
import com.wafa.api.entity.Article;

@Mapper(config = MapStructConfig.class, uses = { UtilisateurMapper.class })
public interface ArticleMapper {

//	@Mapping(source = "articleDto.pieceJointeId", target = "pieceJointe.pieceId")
	@Mapping(source = "articleDto.utilisateurId", target = "utilisateur.utilisateurId")
	Article mapToEntity(ArticleDto articleDto);

	@Mapping(source = "article.utilisateur.utilisateurId", target = "utilisateurId")
//	@Mapping(source = "article.pieceJointe.pieceId", target = "pieceJointeId")
	ArticleDto mapToDTO(Article article);

	List<Article> mapToListOfEntity(List<ArticleDto> articleDtos);

	List<ArticleDto> mapToListOfDTO(List<Article> articles);

}
