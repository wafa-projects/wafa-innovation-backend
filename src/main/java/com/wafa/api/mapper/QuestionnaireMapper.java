package com.wafa.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.wafa.api.dto.QuestionnaireDto;
import com.wafa.api.entity.Questionnaire;

@Mapper(config = MapStructConfig.class, uses = { EnqueteMapper.class , ChoixReponseMapper.class })
public interface QuestionnaireMapper {

	@Mapping(source = "questionnaireDto.enqueteId", target = "enquete.enqueteId")
	Questionnaire mapToEntity(QuestionnaireDto questionnaireDto);
	
	@Mapping(source = "questionnaire.enquete.enqueteId", target = "enqueteId")
	QuestionnaireDto mapToDTO(Questionnaire questionnaire);

	List<Questionnaire> mapToListOfEntity(List<QuestionnaireDto> questionnaireDtos);

	List<QuestionnaireDto> mapToListOfDTO(List<Questionnaire> questionnaires);

}
