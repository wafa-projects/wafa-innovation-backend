package com.wafa.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.wafa.api.dto.AcceuilDto;
import com.wafa.api.entity.AcceuilPage;

@Mapper(config = MapStructConfig.class, uses = { UtilisateurMapper.class })
public interface AcceuilMapper {

//	@Mapping(source = "articleDto.pieceJointeId", target = "pieceJointe.pieceId")
	@Mapping(source = "acceuilDto.utilisateurId", target = "utilisateur.utilisateurId")
	AcceuilPage mapToEntity(AcceuilDto acceuilDto);

	@Mapping(source = "acceuilPage.utilisateur.utilisateurId", target = "utilisateurId")
//	@Mapping(source = "article.pieceJointe.pieceId", target = "pieceJointeId")
	AcceuilDto mapToDTO(AcceuilPage acceuilPage);

	List<AcceuilPage> mapToListOfEntity(List<AcceuilDto> acceuilDtos);

	List<AcceuilDto> mapToListOfDTO(List<AcceuilPage> acceuilPages);

}
