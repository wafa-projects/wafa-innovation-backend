package com.wafa.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.wafa.api.dto.EnqueteDto;
import com.wafa.api.entity.Enquete;

@Mapper(config = MapStructConfig.class, uses = { UtilisateurMapper.class, QuestionnaireMapper.class })
public interface EnqueteMapper {

	@Mapping(source = "enqueteDto.utilisateurId", target = "utilisateur.utilisateurId")
	Enquete mapToEntity(EnqueteDto enqueteDto);

	@Mapping(source = "enquete.utilisateur.utilisateurId", target = "utilisateurId")
	EnqueteDto mapToDTO(Enquete enquete);

	List<Enquete> mapToListOfEntity(List<EnqueteDto> enqueteDtos);

	List<EnqueteDto> mapToListOfDTO(List<Enquete> enquetes);

}
