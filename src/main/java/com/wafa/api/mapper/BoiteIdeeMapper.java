package com.wafa.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.wafa.api.dto.BoiteIdeeDto;
import com.wafa.api.entity.BoiteIdee;

@Mapper(config = MapStructConfig.class, uses = { UtilisateurMapper.class })
public interface BoiteIdeeMapper {

//	@Mapping(source = "boiteIdeeDto.pieceJointeId", target = "pieceJointe.pieceId")
	@Mapping(source = "boiteIdeeDto.utilisateurId", target = "utilisateur.utilisateurId")
	BoiteIdee mapToEntity(BoiteIdeeDto boiteIdeeDto);

	@Mapping(source = "boiteIdee.utilisateur.utilisateurId", target = "utilisateurId")
//	@Mapping(source = "boiteIdee.pieceJointe.pieceId", target = "pieceJointeId")
	BoiteIdeeDto mapToDTO(BoiteIdee boiteIdee);

	List<BoiteIdee> mapToListOfEntity(List<BoiteIdeeDto> boiteIdeeDtos);

	List<BoiteIdeeDto> mapToListOfDTO(List<BoiteIdee> boiteIdee);

}
