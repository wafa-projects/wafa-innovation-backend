package com.wafa.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.wafa.api.dto.ReponseEnqueteDto;
import com.wafa.api.entity.ReponseEnquete;

@Mapper(config = MapStructConfig.class, uses = { UtilisateurMapper.class ,QuestionnaireMapper.class})
public interface ReponseEnqueteMapper {

//	@Mapping(source = "reponseEnqueteDto.choixReponseId", target = "choixReponse.choixId")
	@Mapping(source = "reponseEnqueteDto.utilisateurId", target = "utilisateur.utilisateurId")
	@Mapping(source = "reponseEnqueteDto.questionnaireId", target = "questionnaire.questionnaireId")
	ReponseEnquete mapToEntity(ReponseEnqueteDto reponseEnqueteDto);

	@Mapping(source = "reponseEnquete.utilisateur.utilisateurId", target = "utilisateurId")
	@Mapping(source = "reponseEnquete.questionnaire.questionnaireId", target = "questionnaireId")
//	@Mapping(source = "reponseEnquete.choixReponse.choixId", target = "choixReponseId")
	ReponseEnqueteDto mapToDTO(ReponseEnquete reponseEnquete);

	List<ReponseEnquete> mapToListOfEntity(List<ReponseEnqueteDto> reponseEnqueteDtos);

	List<ReponseEnqueteDto> mapToListOfDTO(List<ReponseEnquete> reponseEnquetes);

}
