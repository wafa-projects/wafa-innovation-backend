package com.wafa.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.wafa.api.dto.ReponseDto;
import com.wafa.api.entity.Reponse;

@Mapper(config = MapStructConfig.class, uses = { UtilisateurMapper.class ,QuestionMapper.class })
public interface ReponseMapper {
	
	@Mapping(source = "reponseDto.questionId", target = "question.questionId")
	@Mapping(source = "reponseDto.utilisateurId", target = "utilisateur.utilisateurId")
	Reponse mapToEntity(ReponseDto reponseDto);

	@Mapping(source = "reponse.utilisateur.utilisateurId", target = "utilisateurId")
	@Mapping(source = "reponse.question.questionId", target = "questionId")
	ReponseDto mapToDTO(Reponse reponse);

	List<Reponse> mapToListOfEntity(List<ReponseDto> reponseDtos);

	List<ReponseDto> mapToListOfDTO(List<Reponse> reponses);

}
