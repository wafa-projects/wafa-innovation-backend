package com.wafa.api.exception;



public class NotFoundException extends RuntimeException {

  private static final long serialVersionUID = -3372433568932641320L;


  public NotFoundException(final String message, final Throwable e) {
    super(message, e);
  }


  public NotFoundException(final String message) {
    super(message);
  }

}
