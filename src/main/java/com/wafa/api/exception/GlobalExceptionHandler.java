package com.wafa.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(value = { TechnicalException.class })
	protected ResponseEntity<Error> handleTechnicalException(RuntimeException ex, WebRequest request) {
		Error error = new Error();
		error.setCode(408);
		error.setMessage(ex.getMessage());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);

	}

	@ExceptionHandler(value = { NotFoundException.class })
	protected ResponseEntity<Error> handleNotFoundException(RuntimeException ex, WebRequest request) {
		Error error = new Error();
		error.setCode(404);
		error.setMessage(ex.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);

	}

	@ExceptionHandler(value = { IllegalArgumentException.class })
	protected ResponseEntity<Error> handleIllegalArgumentException(RuntimeException ex, WebRequest request) {
		Error error = new Error();
		error.setCode(HttpStatus.BAD_REQUEST.value());
		error.setMessage(ex.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);

	}

}
