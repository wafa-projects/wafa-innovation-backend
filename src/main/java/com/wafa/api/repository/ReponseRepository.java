package com.wafa.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.wafa.api.entity.Reponse;


public interface ReponseRepository extends JpaRepository<Reponse, Long>, JpaSpecificationExecutor<Reponse> {
	
	List<Reponse> findByUtilisateurUtilisateurId(Long id);
	List<Reponse> findByQuestionQuestionId(Long id);

}
