package com.wafa.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wafa.api.entity.AcceuilPage;


public interface AcceuilPageRepository extends JpaRepository<AcceuilPage, Long> {

}
