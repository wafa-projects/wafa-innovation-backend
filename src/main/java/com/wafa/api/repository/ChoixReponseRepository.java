package com.wafa.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.wafa.api.entity.ChoixReponse;


public interface ChoixReponseRepository extends JpaRepository<ChoixReponse, Long>, JpaSpecificationExecutor<ChoixReponse> {

	List<ChoixReponse> findByQuestionnaireQuestionnaireId(Long id);
}
