package com.wafa.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.wafa.api.entity.Commentaire;

public interface CommentaireRepository extends JpaRepository<Commentaire, Long> ,JpaSpecificationExecutor<Commentaire> {

	List<Commentaire> findByArticleArticleId(Long id);

	List<Commentaire> findByArticleArticleIdAndIsVisible(Long id, Boolean visible);

}
