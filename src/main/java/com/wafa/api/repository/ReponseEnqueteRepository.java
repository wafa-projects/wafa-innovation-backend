package com.wafa.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.wafa.api.entity.ReponseEnquete;


public interface ReponseEnqueteRepository extends JpaRepository<ReponseEnquete, Long>, JpaSpecificationExecutor<ReponseEnquete> {

	List<ReponseEnquete> findByUtilisateurUtilisateurId(Long id);
	
	List<ReponseEnquete> findByQuestionnaireQuestionnaireId(Long id);
	
	ReponseEnquete findByQuestionnaireQuestionnaireIdAndUtilisateurUtilisateurId(Long id,Long idUser);
}
