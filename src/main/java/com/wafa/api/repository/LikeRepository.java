package com.wafa.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wafa.api.entity.ActionLike;


public interface LikeRepository extends JpaRepository<ActionLike, Long> {

	List<ActionLike> findByArticleArticleId(Long id);
	
	ActionLike findByArticleArticleIdAndUtilisateurUtilisateurId(Long idA,Long idUser);
	
	long countByArticleArticleId(Long id);

}
