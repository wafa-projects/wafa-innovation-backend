package com.wafa.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.wafa.api.entity.BoiteIdee;


public interface BoiteIdeeRepository extends JpaRepository<BoiteIdee, Long>, JpaSpecificationExecutor<BoiteIdee> {

	List<BoiteIdee> findByUtilisateurUtilisateurId(Long id);
	
	Long countByUtilisateurUtilisateurId(Long id);
}
