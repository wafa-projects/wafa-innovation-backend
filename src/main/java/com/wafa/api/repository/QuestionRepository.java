package com.wafa.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.wafa.api.entity.Question;


public interface QuestionRepository extends JpaRepository<Question, Long>, JpaSpecificationExecutor<Question> {

	List<Question> findByUtilisateurUtilisateurId(Long id);
	
	List<Question> findByQuestionParentQuestionId(Long id);
	
	long countByUtilisateurUtilisateurId(Long id);
	
}
