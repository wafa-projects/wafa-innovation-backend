package com.wafa.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.wafa.api.entity.Enquete;


public interface EnqueteRepository extends JpaRepository<Enquete, Long>, JpaSpecificationExecutor<Enquete> {
	
	List<Enquete> findByUtilisateurUtilisateurId(Long id);

	 
}
