package com.wafa.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.wafa.api.entity.PieceJointe;


public interface PieceJointeRepository extends JpaRepository<PieceJointe, Long>, JpaSpecificationExecutor<PieceJointe> {

}
