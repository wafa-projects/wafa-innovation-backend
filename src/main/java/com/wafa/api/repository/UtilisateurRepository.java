package com.wafa.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.wafa.api.entity.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long>, JpaSpecificationExecutor<Utilisateur> {

	Utilisateur findByEmailAndPasswordAndIsDeleted(String email, String password,Boolean delete);
	
	List<Utilisateur> findByIsDeleted(Boolean delete);
	
	Utilisateur findByEmail(String email);

	@Query(value = "select count(*) from enquete_participants e where e.participants_id_utilisateur = ?1", nativeQuery = true)
	Long countPartcipation(Long idUser);

}
