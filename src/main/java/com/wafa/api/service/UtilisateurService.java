package com.wafa.api.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.ws.rs.core.Response;

import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.wafa.api.config.SecurityContextUtils;
import com.wafa.api.dto.TokenOpenIdDto;
import com.wafa.api.dto.UserDto;
import com.wafa.api.dto.UtilisateurDto;
import com.wafa.api.dto.ValidationDTO;
import com.wafa.api.entity.Utilisateur;
import com.wafa.api.exception.NotFoundException;
import com.wafa.api.exception.TechnicalException;
import com.wafa.api.mapper.UtilisateurMapper;
import com.wafa.api.repository.ArticleRepository;
import com.wafa.api.repository.BoiteIdeeRepository;
import com.wafa.api.repository.QuestionRepository;
import com.wafa.api.repository.UtilisateurRepository;

import lombok.extern.slf4j.Slf4j;
import specification.UtilisateurSpecefication;
import specification.filter.UserFilter;

@Slf4j
@Service
public class UtilisateurService {

	@Autowired
	private UtilisateurRepository repository;

	@Autowired
	private UtilisateurMapper mapper;

	@Autowired
	private EmailService emailService;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ArticleRepository articleRepository;

	@Autowired
	private QuestionRepository questionRepository;

	@Autowired
	private BoiteIdeeRepository boiteIdeeRepository;

	@Value("${app.url.server}")
	private String serviceFront;

	@Value("${email.pattern}")
	private String emailPattern;

	@Value("${storage.path.root}")
	private String rootStorage;

	@Value("${keycloak.auth-server-url}")
	private String serverUrl;

	@Value("${keycloak.realm}")
	private String realm;

	@Value("${keycloak.resource}")
	private String clientId;

	@Value("${security.oauth2.client.client-secret}")
	private String clientSecret;

	@Value("${application.username}")
	private String username;

	@Value("${application.password}")
	private String password;

	@Value("${app.icap.icapservice}")
	private String icapService;
	@Value("${app.icap.serverip}")
	private String serverIP;
	@Value("${app.icap.port}")
	private int port;

	@Transactional(readOnly = true)
	public List<UtilisateurDto> getAllUtilisateur() {

		log.info("L'affichage des Utilisateurs");
		List<Utilisateur> utilisateurs = repository.findAll();
		return mapper.mapToListOfDTO(utilisateurs);
	}

	@Transactional(readOnly = true)
	public List<UtilisateurDto> getAllUtilisateurNotDeleted() {

		log.info("L'affichage des Utilisateurs");
		List<Utilisateur> utilisateurs = repository.findByIsDeleted(false);
		return mapper.mapToListOfDTO(utilisateurs);
	}

	@Transactional
	public ValidationDTO uploadFichierPiece(MultipartFile file, Long id) throws IOException {
		if (file != null) {
			ValidationDTO validationJson = new ValidationDTO();
			try {
				// Starts a connection to the ICAP server
				ICAP icap = new ICAP(serverIP, port, icapService);
				try {
					Utilisateur utilisateur = repository.findById(id).orElse(null);
					if (utilisateur == null) {
						throw new NotFoundException("L'utilisateur " + id + " est inexistant");
					}
					// pieceJointe.setFichier(file.getBytes());
					if (file.getSize() > 60000000) {
						throw new IllegalArgumentException("Le Fichier est trop volumineux");
					}

					Boolean scan = icap.scanStream(file.getInputStream());
					//Boolean scan = icap.scanStream("C:\\Users\\AYOUB\\Desktop\\img\\liver.jpg");

					if (!scan) {
						throw new IllegalArgumentException("Ce fichier est un contenut malveillant ");
					}

					utilisateur.setPhotoProfile(
							"/photoUser/" + utilisateur.getUtilisateurId() + "." + file.getContentType().split("/")[1]);
					byte[] bytes = file.getBytes();
					File folder = new File(rootStorage + "/photoUser/");
					if (!folder.exists())
						folder.mkdirs();
					Path path = Paths.get(rootStorage + "/photoUser/" + utilisateur.getUtilisateurId() + "."
							+ file.getContentType().split("/")[1]);
					Files.write(path, bytes);
					Utilisateur fileCreated = repository.saveAndFlush(utilisateur);
					validationJson.setId(fileCreated.getUtilisateurId());
					validationJson.setMessage("File uploaded successfully");
					return validationJson;
				} catch (Exception e) {
					throw new TechnicalException("FAIL! Problème au niveau upload du fichier");

				}
			} catch (Exception e) {
				throw new TechnicalException("Error occurred when connecting" + e.getMessage());

			}
		} else {
			throw new IllegalArgumentException("Object canot be null");
		}

	}

	@Transactional(readOnly = true)
	public byte[] getImageUserById(Long id) throws IOException {

		Utilisateur utilisateur = repository.findById(id).orElse(null);
		if (utilisateur == null) {
			throw new NotFoundException("L'utilisateur " + id + " est inexistant");
		}

		File file = new File(rootStorage + utilisateur.getPhotoProfile());

		InputStream targetStream = new FileInputStream(file);
		byte[] bytes = StreamUtils.copyToByteArray(targetStream);

		return bytes;
	}

	public Boolean checkEmailExist(String email) {
		Boolean check = false;
		if (email != null) {
			Utilisateur user = repository.findByEmail(email);
			if (user != null) {
				check = true;
			}
		} else {
			throw new NotFoundException("l'email est inexistant");
		}
		return check;
	}

	public Boolean checkEmailPattern(String email) {
		if (emailPattern == null || emailPattern.equals("")) {
			return true;
		} else {
			if (email.endsWith(emailPattern)) {
				return true;
			} else {
				return false;
			}
		}
	}

	@Transactional
	public UtilisateurDto saveUtilisateur(UtilisateurDto utilisateurDto) {
		if (utilisateurDto != null) {
			log.info("La Creation d'un utilisateur ");
			if (checkEmailExist(utilisateurDto.getEmail())) {
				throw new TechnicalException("Cette email existe déja");
			}
			if (!checkEmailPattern(utilisateurDto.getEmail())) {
				throw new TechnicalException("Cette email n'existe pas valide chez wafasalaf");
			}
			utilisateurDto.setRole("USER");
			utilisateurDto.setStatut("init");
			Utilisateur utilisateur = mapper.mapToEntity(utilisateurDto);

			Utilisateur savedUtilisateur = repository.save(utilisateur);
			UtilisateurDto savedUtilisateurDto = mapper.mapToDTO(savedUtilisateur);
			createUserKeycloak(savedUtilisateurDto);
			String msg = "Bonjour,  \n \n";
			msg += "Nous vous confirmons la réception de votre demande d'inscription au programme d'innovation ETINCELLE et vous en remercions. \n \n";
			msg += "Afin de compléter votre inscription et accéder à votre compte merci de cliquer sur le lien ci-après : \n \n";
			msg += serviceFront + "/#/confirmation/" + savedUtilisateurDto.getUtilisateurId()
					+ "/022062844945994994049040777089890990000908887777000066060 \n \n";
			msg += "Pour accéder à votre compte, veuillez trouver ci-après vos identifiants: \n \n";
			msg += "Votre Login :" + savedUtilisateurDto.getEmail() + "\n";
			msg += "Votre mot de pass :" + savedUtilisateurDto.getPassword() + "\n \n";
			msg += "Cordialement, \n";
			msg += "Equipe Innovation";
			emailService.sendSimpleMail(savedUtilisateur.getEmail(), "Inscription", msg);
			return savedUtilisateurDto;

		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de la creation d'une utilisateur");
			throw new IllegalArgumentException(
					"Une Erreur Fonctionnelle s'est produite lors de la creation d'une utilisateur");
		}
	}

	@Transactional
	public void deleteUser(Long id) {
		if (id != null) {
			Utilisateur utilisateur = repository.findById(id).orElse(null);
			if (utilisateur == null) {
				throw new NotFoundException("l'utilisateur " + id + " est inexistant");
			}
			repository.deleteById(id);
		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la suppression d'un utilisateur");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la suppression d'un utilisateur");
		}
	}

	@Transactional
	public UtilisateurDto updateUtilisateur(UtilisateurDto utilisateurDto, Long id) {
		if (utilisateurDto != null && id != null) {
			Utilisateur utilisateur = repository.findById(id).orElse(null);
			if (utilisateur == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}

			utilisateurDto.setUtilisateurId(id);
			utilisateurDto.setPassword(utilisateur.getPassword());
			Utilisateur utilisateurUpdate = mapper.mapToEntity(utilisateurDto);
			Utilisateur savedUtilisateur = repository.saveAndFlush(utilisateurUpdate);
			return mapper.mapToDTO(savedUtilisateur);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification d'un utilisateur");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la modification d'un utilisateur");
		}
	}

	@Transactional
	public Boolean forgetPassword(String email) {
		log.info("Récupération de password user");

		Utilisateur user = repository.findByEmail(email);
		if (user != null) {

			UtilisateurDto utilisateurDto = mapper.mapToDTO(user);
			String msg = "Bonjour,  \n \n";
			msg += "Pour accéder à votre compte, veuillez trouver ci-après vos identifiants: \n \n";
			msg += "Votre Login :" + utilisateurDto.getEmail() + "\n";
			msg += "Votre mot de pass :" + utilisateurDto.getPassword() + "\n \n";
			msg += "Cordialement, \n";
			msg += "Equipe Innovation";
			emailService.sendSimpleMail(email, "Identifiants", msg);
			return true;
		} else {
			throw new TechnicalException("Cette email n'existe pas");

		}

	}

	@Transactional(readOnly = true)
	public Page<UtilisateurDto> getUserByCriteria(UserFilter userFilter) {
		List<Utilisateur> result = new ArrayList<>();
		log.info("Afficher la list des user avec Pagination");

		Pageable pageable = PageRequest.of(userFilter.getPage(), userFilter.getSize());

		Page<Utilisateur> pageArticle = repository
				.findAll(Specification.where(UtilisateurSpecefication.getById(userFilter.getUtilisateurId()))
						.and(UtilisateurSpecefication.getByNom(userFilter.getNom()))
						.and(UtilisateurSpecefication.getDeletedUser(userFilter.getIsDeleted()))
						.and(UtilisateurSpecefication.getByMatricule(userFilter.getMatricule()))
						.and(UtilisateurSpecefication.getByStatutUser(userFilter.getStatut()))
						.and(UtilisateurSpecefication.getByEmail(userFilter.getEmail())), pageable);

		pageArticle.forEach(result::add);

		List<UtilisateurDto> listUtilisateurDto = mapper.mapToListOfDTO(result);

		return new PageImpl<>(listUtilisateurDto, pageable, pageArticle.getTotalElements());
	}

	@Transactional
	public UtilisateurDto validereUtilisateur(Long id) {
		if (id != null) {
			Utilisateur utilisateur = repository.findById(id).orElse(null);
			if (utilisateur == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}

			utilisateur.setStatut("valide");
			Utilisateur savedUtilisateur = repository.saveAndFlush(utilisateur);
			return mapper.mapToDTO(savedUtilisateur);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification d'un utilisateur");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la modification d'un utilisateur");
		}
	}

	@Transactional
	public UtilisateurDto deleteLogicUser(Long id) {
		if (id != null) {
			Utilisateur utilisateur = repository.findById(id).orElse(null);
			if (utilisateur == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}

			utilisateur.setIsDeleted(true);
			Utilisateur savedUtilisateur = repository.saveAndFlush(utilisateur);
			return mapper.mapToDTO(savedUtilisateur);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification d'un utilisateur");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la modification d'un utilisateur");
		}
	}

	@Transactional(readOnly = true)
	public Long countPartcipation(Long id) {
		if (null != id) {

			log.info("L'affichage d'une utilisateur a travers son Identifiant");
			Utilisateur utilisateur = repository.findById(id).orElse(null);
			if (utilisateur == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}
			return repository.countPartcipation(id);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage d'une utilisateur");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional
	public UtilisateurDto getUtilisateurByToken(String user) {
		if (null != user) {

			log.info("L'affichage d'une utilisateur a travers son Identifiant");
			Utilisateur utilisateur = repository.findByEmail(user);
			if (utilisateur == null) {
				throw new NotFoundException("l'utilisateur " + user + " Est inexistant");
			}
			UserDto userDto = UserDto.builder().nbrEnquete(repository.countPartcipation(utilisateur.getUtilisateurId()))
					.nbrArticle(articleRepository.countByUtilisateurUtilisateurId(utilisateur.getUtilisateurId()))
					.nbrQuestion(questionRepository.countByUtilisateurUtilisateurId(utilisateur.getUtilisateurId()))
					.nbrIdee(boiteIdeeRepository.countByUtilisateurUtilisateurId(utilisateur.getUtilisateurId()))
					.build();
			UtilisateurDto utilisateurDto = mapper.mapToDTO(utilisateur);
			utilisateurDto.setUserDto(userDto);
			Set<String> roles = SecurityContextUtils.getUserRoles();

			if (roles.contains("ROLE_ADMIN")) {
				utilisateurDto.setRole("ADMIN");
			} else {
				utilisateurDto.setRole("USER");
			}
			updateUtilisateur(utilisateurDto, utilisateur.getUtilisateurId());
			return utilisateurDto;
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage d'une utilisateur");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional(readOnly = true)
	public UtilisateurDto getUtilisateurById(Long id) {
		if (null != id) {

			log.info("L'affichage d'une utilisateur a travers son Identifiant");
			Utilisateur utilisateur = repository.findById(id).orElse(null);
			if (utilisateur == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}
			UserDto userDto = UserDto.builder().nbrEnquete(repository.countPartcipation(id))
					.nbrArticle(articleRepository.countByUtilisateurUtilisateurId(id))
					.nbrQuestion(questionRepository.countByUtilisateurUtilisateurId(id))
					.nbrIdee(boiteIdeeRepository.countByUtilisateurUtilisateurId(id)).build();
			UtilisateurDto utilisateurDto = mapper.mapToDTO(utilisateur);
			utilisateurDto.setUserDto(userDto);

			// Set<String> roles = SecurityContextUtils.getUserRoles();
			// if (roles.contains("ROLE_ADMIN")) {
			// utilisateurDto.setRole("ADMIN");
			// }
			// else{
			// utilisateurDto.setRole("USER");
			// }
			return utilisateurDto;
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage d'une utilisateur");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional(readOnly = true)
	public Page<UtilisateurDto> getUtilisateurPagable(int page, int size) {
		List<Utilisateur> utilisateurs = repository.findAll();

		List<UtilisateurDto> listUtilisateurDto = mapper.mapToListOfDTO(utilisateurs);

		int start = (int) PageRequest.of(page, size).getOffset();
		int end = (start + PageRequest.of(page, size).getPageSize()) > listUtilisateurDto.size()
				? listUtilisateurDto.size() : (start + PageRequest.of(page, size).getPageSize());

		return new PageImpl<>(listUtilisateurDto.subList(start, end), PageRequest.of(page, size),
				listUtilisateurDto.size());

	}

	@Transactional(readOnly = true)
	public UtilisateurDto getUserByEmailAndPassword(String email, String password) {
		if (null != email && password != null) {

			log.info("L'affichage d'une utilisateur a travers son Identifiant");
			Utilisateur utilisateur = repository.findByEmailAndPasswordAndIsDeleted(email, password, false);

			if (utilisateur == null) {
				throw new NotFoundException("Email ou mot pass est incorrecte");
			}
			UserDto userDto = UserDto.builder().nbrEnquete(repository.countPartcipation(utilisateur.getUtilisateurId()))
					.nbrArticle(articleRepository.countByUtilisateurUtilisateurId(utilisateur.getUtilisateurId()))
					.nbrQuestion(questionRepository.countByUtilisateurUtilisateurId(utilisateur.getUtilisateurId()))
					.nbrIdee(boiteIdeeRepository.countByUtilisateurUtilisateurId(utilisateur.getUtilisateurId()))
					.build();

			try {

				HttpHeaders headers = new HttpHeaders();
				headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED.toString());
				headers.add("Accept", MediaType.APPLICATION_JSON.toString());
				LinkedMultiValueMap<String, String> data = new LinkedMultiValueMap<String, String>();
				data.add("client_id", clientId);
				data.add("client_secret", clientSecret);
				data.add("grant_type", "password");
				data.add("username", email);
				data.add("password", password);
				HttpEntity<LinkedMultiValueMap<String, String>> formEntity = new HttpEntity<LinkedMultiValueMap<String, String>>(
						data, headers);

				ResponseEntity<TokenOpenIdDto> responseKeyClock = restTemplate.exchange(
						serverUrl + "/realms/wafa-realm/protocol/openid-connect/token", HttpMethod.POST, formEntity,
						TokenOpenIdDto.class);

				if (responseKeyClock.getStatusCode() == HttpStatus.FORBIDDEN) {
					throw new NotFoundException("Email ou mot pass est incorrecte");
				}

				UtilisateurDto utilisateurDto = mapper.mapToDTO(utilisateur);
				utilisateurDto.setUserDto(userDto);
				utilisateurDto.setPassword("");
				utilisateurDto.setTockenAuth(responseKeyClock.getBody());
				return utilisateurDto;
			} catch (RestClientException ex) {
				throw new IllegalArgumentException("Problème lors de connection au serveur d'authentification ");
			}

		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage d'une utilisateur");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}

	}

	public void createUserKeycloak(UtilisateurDto utilisateurDto) {

		Keycloak keycloak = KeycloakBuilder.builder() //
				.serverUrl(serverUrl) //
				.realm(realm) //
				.grantType(OAuth2Constants.PASSWORD) //
				.clientId(clientId) //
				.clientSecret(clientSecret) //
				.username(username) //
				.password(password) //
				.build();

		// Define user
		UserRepresentation user = new UserRepresentation();
		user.setEnabled(true);
		user.setUsername(utilisateurDto.getEmail());
		user.setFirstName(utilisateurDto.getPrenom());
		user.setLastName(utilisateurDto.getNom());
		user.setEmail(utilisateurDto.getEmail());

		// Get realm
		RealmResource realmResource = keycloak.realm(realm);
		UsersResource usersRessource = realmResource.users();

		// Create user (requires manage-users role)
		Response response = usersRessource.create(user);

		String userId = CreatedResponseUtil.getCreatedId(response);

		// Define password credential
		CredentialRepresentation passwordCred = new CredentialRepresentation();
		passwordCred.setTemporary(false);
		passwordCred.setType(CredentialRepresentation.PASSWORD);
		passwordCred.setValue(utilisateurDto.getPassword());

		UserResource userResource = usersRessource.get(userId);

		// Set password credential
		userResource.resetPassword(passwordCred);

		String userRole = "app-admin";

		List<RoleRepresentation> roleRepresentationList = userResource.roles().realmLevel().listAvailable();

		for (RoleRepresentation roleRepresentation : roleRepresentationList) {
			if (roleRepresentation.getName().equals(userRole)) {
				userResource.roles().realmLevel().add(Arrays.asList(roleRepresentation));
				break;
			}
		}

		// List<ClientRepresentation> app1Client =
		// realmResource.clients().findAll();
		//
		// System.out.println("taille ---->" + app1Client.size());
		// log.info("taille ---->" + app1Client.size());

		ClientRepresentation clientRep = realmResource.clients().findByClientId(clientId).get(0);
		RoleRepresentation clientRoleRep = realmResource.clients().get(clientRep.getId()).roles().get("USER")
				.toRepresentation();
		userResource.roles().clientLevel(clientRep.getId()).add(Arrays.asList(clientRoleRep));

	}

}
