package com.wafa.api.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.wafa.api.dto.BoiteIdeeDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BoiteIdeeExcel {

	@Autowired
	private BoiteIdeeService boiteIdeeService;
	
	@Value("${app.url.download}")
	private String lien;

	private int cpt = 0;

	public void createTitles(Sheet sheet, Workbook workbook, int cp) {

		Row row = sheet.createRow(cp); // row 1
		CellStyle headerStyle = workbook.createCellStyle();

		headerStyle.setBorderBottom(BorderStyle.THIN);
		headerStyle.setBorderTop(BorderStyle.THIN);
		headerStyle.setBorderLeft(BorderStyle.THIN);
		headerStyle.setBorderRight(BorderStyle.THIN);

		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		XSSFFont font = ((XSSFWorkbook) workbook).createFont();
		headerStyle.setFont(font);

		Cell cell = row.createCell(0); // cell A1
		headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		cell.setCellValue("Nom");
		cell.setCellStyle(headerStyle);
		// set vertical alignment center
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);

		cell = row.createCell(1); // cell B1
		cell.setCellValue("Prénom");
		// set horizontal alignment center
		cell.setCellStyle(headerStyle);
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);

		cell = row.createCell(2); // cell B1
		cell.setCellValue("Matricule");
		// set horizontal alignment center
		cell.setCellStyle(headerStyle);
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);

		cell = row.createCell(3); // cell B1
		cell.setCellValue("Entité");
		cell.setCellStyle(headerStyle);
		// set horizontal alignment center
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);

		cell = row.createCell(4); // cell B1
		cell.setCellValue("Ville");
		cell.setCellStyle(headerStyle);
		// set horizontal alignment center
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);

		cell = row.createCell(5); // cell B1
		cell.setCellValue("Région");
		cell.setCellStyle(headerStyle);
		// set horizontal alignment center
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);

		cell = row.createCell(6); // cell B1
		cell.setCellValue("Idée");
		cell.setCellStyle(headerStyle);
		// set horizontal alignment center
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
		
		cell = row.createCell(7); // cell B1
		cell.setCellValue("Date d'idée");
		cell.setCellStyle(headerStyle);
		// set horizontal alignment center
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
		
		cell = row.createCell(8); // cell B1
		cell.setCellValue("Fichier téléchargé");
		cell.setCellStyle(headerStyle);
		// set horizontal alignment center
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
		
		cpt++;

	}

	public void createListFields(Sheet sheet, Workbook workbook, List<BoiteIdeeDto> boiteIdeeDtos) {
		
		for (BoiteIdeeDto boiteIdeeDto : boiteIdeeDtos) {
			Row row = sheet.createRow(cpt); // row 1
			CellStyle headerStyle = workbook.createCellStyle();
			headerStyle.setBorderBottom(BorderStyle.THIN);
			headerStyle.setBorderTop(BorderStyle.THIN);
			headerStyle.setBorderLeft(BorderStyle.THIN);
			headerStyle.setBorderRight(BorderStyle.THIN);

			Cell cell = row.createCell(0); // cell A1

			cell.setCellValue(boiteIdeeDto.getUtilisateur().getNom());
			CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
			cell.setCellStyle(headerStyle);

			cell = row.createCell(1); // cell B1
			cell.setCellValue(boiteIdeeDto.getUtilisateur().getPrenom());
			// set horizontal alignment center
			CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
			cell.setCellStyle(headerStyle);

			cell = row.createCell(2); // cell B1
			cell.setCellValue(boiteIdeeDto.getUtilisateur().getMatricule());
			// set horizontal alignment center
			CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
			cell.setCellStyle(headerStyle);

			cell = row.createCell(3); // cell B1
			cell.setCellValue(boiteIdeeDto.getUtilisateur().getEntitee());
			// set horizontal alignment center
			CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
			cell.setCellStyle(headerStyle);

			cell = row.createCell(4); // cell B1
			cell.setCellValue(boiteIdeeDto.getUtilisateur().getVille());
			// set horizontal alignment center
			CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
			cell.setCellStyle(headerStyle);

			cell = row.createCell(5); // cell B1
			cell.setCellValue(boiteIdeeDto.getUtilisateur().getRegion());
			// set horizontal alignment center
			CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
			cell.setCellStyle(headerStyle);

			cell = row.createCell(6); // cell B1
			cell.setCellValue(boiteIdeeDto.getDescription());
			// set horizontal alignment center
			CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
			cell.setCellStyle(headerStyle);
			
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			cell = row.createCell(7); // cell B1
			cell.setCellValue(boiteIdeeDto.getDateIdee().format(formatter));
			// set horizontal alignment center
			CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
			cell.setCellStyle(headerStyle);
			
			cell = row.createCell(8); // cell B1
			String msg="";
			if(boiteIdeeDto.getPieceJointe()!=null){
				msg=lien+boiteIdeeDto.getPieceJointe().getPieceId();
			}
			cell.setCellValue(msg);
			// set horizontal alignment center
			CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
			cell.setCellStyle(headerStyle);
			cpt++;
		}

	}

	public ByteArrayInputStream writeExcel() throws IOException {
		log.info("generate idéee excel");
		Workbook workbook = new XSSFWorkbook();
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {
			Sheet sheet = workbook.createSheet("Idées");

			List<BoiteIdeeDto> boiteIdeeDtos = boiteIdeeService.getAllBoiteIdee();
			for (int i = 0; i <= 7; i++) {
				sheet.setColumnWidth(i, 9000);
			}

			cpt = 0;

			createTitles(sheet, workbook, cpt);
			if (boiteIdeeDtos != null && !boiteIdeeDtos.isEmpty()) {

				createListFields(sheet, workbook, boiteIdeeDtos);

			}
			workbook.write(out);
		} finally {
			if (workbook != null) {

				workbook.close();

			}
		}
		return new ByteArrayInputStream(out.toByteArray());
	}

}
