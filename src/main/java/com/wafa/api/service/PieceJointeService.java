package com.wafa.api.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartFile;

import com.wafa.api.dto.ValidationDTO;
import com.wafa.api.entity.PieceJointe;
import com.wafa.api.exception.NotFoundException;
import com.wafa.api.exception.TechnicalException;
import com.wafa.api.repository.PieceJointeRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PieceJointeService {

	@Autowired
	private PieceJointeRepository repository;

	@Value("${storage.path.root}")
	private String rootStorage;
	
	@Value("${app.icap.icapservice}")
	private String icapService;
	@Value("${app.icap.serverip}")
	private String serverIP;
	@Value("${app.icap.port}")
	private int port;

	@Transactional(readOnly = true)
	public List<PieceJointe> getAllPieceJointe() {

		log.info("L'affichage des PieceJointes");
		List<PieceJointe> pieceJointes = repository.findAll();
		return pieceJointes;
	}

	@Transactional
	public void deletePieceJointe(Long id) {
		if (id != null) {
			PieceJointe pieceJointe = repository.findById(id).orElse(null);
			if (pieceJointe == null) {
				throw new NotFoundException("La pieceJointe " + id + " est inexistant");
			}
			repository.deleteById(id);
		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la suppression d'une pieceJointe");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la suppression d'une pieceJointe");
		}
	}

	@Transactional
	public ValidationDTO uploadFichierPiece(MultipartFile file, Long id) throws IOException {
		if (file != null) {
			ValidationDTO validationJson = new ValidationDTO();
			try {
				// Starts a connection to the ICAP server
				ICAP icap = new ICAP(serverIP, port, icapService);
				try {
					PieceJointe pieceJointe = repository.findById(id).orElse(null);
					// pieceJointe.setFichier(file.getBytes());
					if (file.getSize() > 60000000) {
						throw new IllegalArgumentException("Le Fichier est trop volumineux");
					}
					Boolean scan=icap.scanStream(file.getInputStream());
					if(!scan){
						throw new IllegalArgumentException("Ce fichier est un contenut malveillant ");
					}
					
					pieceJointe.setLien("/filesBoite/" + id + "." + file.getContentType().split("/")[1]);
					pieceJointe.setType(file.getContentType().split("/")[1]);
					byte[] bytes = file.getBytes();
					File folder = new File(rootStorage + "/filesBoite/");
					if (!folder.exists())
						folder.mkdirs();

					Path path = Paths
							.get(rootStorage + "/filesBoite/" + id + "." + file.getContentType().split("/")[1]);

					Files.write(path, bytes);
					PieceJointe fileCreated = repository.saveAndFlush(pieceJointe);
					validationJson.setId(fileCreated.getPieceId());
					validationJson.setMessage("File uploaded successfully");
					return validationJson;
				} catch (Exception e) {
					throw new TechnicalException("FAIL! Problème au niveau upload du fichier");

				}
			} catch (Exception e) {
				throw new TechnicalException("Error occurred when connecting" + e.getMessage());

			}
		} else {
			throw new IllegalArgumentException("Object canot be null");
		}

	}

	@Transactional(readOnly = true)
	public byte[] getFilePieceById(Long id) throws IOException {

		PieceJointe pieceJointe = repository.findById(id).orElse(null);
		if (pieceJointe == null) {
			throw new NotFoundException("La pieceJointe " + id + " est inexistant");
		}

		File file = new File(rootStorage + pieceJointe.getLien());

		InputStream targetStream = new FileInputStream(file);
		byte[] bytes = StreamUtils.copyToByteArray(targetStream);

		return bytes;
	}

	@Transactional(readOnly = true)
	public ByteArrayInputStream downloadPieceById(Long id) throws IOException {

		PieceJointe pieceJointe = repository.findById(id).orElse(null);
		if (pieceJointe == null) {
			throw new NotFoundException("La pieceJointe " + id + " est inexistant");
		}

		File file = new File(rootStorage + pieceJointe.getLien());

		InputStream targetStream = new FileInputStream(file);
		byte[] bytes = StreamUtils.copyToByteArray(targetStream);

		return new ByteArrayInputStream(bytes);
	}

	@Transactional
	public PieceJointe savePieceJointe(PieceJointe pieceJointe) {
		if (pieceJointe != null) {
			log.info("La Creation d'un pieceJointe ");

			PieceJointe savedPieceJointe = repository.save(pieceJointe);
			return savedPieceJointe;

		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de la creation d'une pieceJointe");
			throw new IllegalArgumentException(
					"Une Erreur Fonctionnelle s'est produite lors de la creation d'une pieceJointe");
		}
	}

	@Transactional(readOnly = true)
	public PieceJointe getPieceJointeById(Long id) {
		if (null != id) {

			log.info("L'affichage d'une pieceJointe a travers son Identifiant");
			PieceJointe pieceJointe = repository.findById(id).orElse(null);
			if (pieceJointe == null) {
				throw new NotFoundException("l'pieceJointe " + id + " Est inexistant");
			}
			return pieceJointe;
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage d'une pieceJointe");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

}
