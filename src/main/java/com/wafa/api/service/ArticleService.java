package com.wafa.api.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wafa.api.config.SecurityContextUtils;
import com.wafa.api.dto.ArticleDto;
import com.wafa.api.entity.ActionLike;
import com.wafa.api.entity.Article;
import com.wafa.api.entity.Commentaire;
import com.wafa.api.entity.Utilisateur;
import com.wafa.api.exception.NotFoundException;
import com.wafa.api.mapper.ArticleMapper;
import com.wafa.api.repository.ArticleRepository;
import com.wafa.api.repository.CommentaireRepository;
import com.wafa.api.repository.LikeRepository;
import com.wafa.api.repository.UtilisateurRepository;

import lombok.extern.slf4j.Slf4j;
import specification.ArticleSpecifications;
import specification.filter.ArticleFilter;

@Slf4j
@Service
public class ArticleService {

	@Autowired
	private ArticleRepository repository;

	@Autowired
	private UtilisateurRepository userRepository;

	@Autowired
	private UtilisateurService userService;

	@Autowired
	private ArticleMapper mapper;

	@Autowired
	private LikeRepository likeRepository;

	@Autowired
	private EmailService emailService;

	@Autowired
	private CommentaireRepository commentaireRepository;

	@Transactional(readOnly = true)
	public List<ArticleDto> getAllArticle() {

		log.info("L'affichage des Articles");
		List<Article> articles = repository.findAll();
		return mapper.mapToListOfDTO(articles);
	}

	@Transactional(readOnly = true)
	public Page<ArticleDto> getArticleByCriteria(ArticleFilter articleFilter) {
		List<Article> result = new ArrayList<>();
		log.info("Afficher la list des article avec Pagination");

		Pageable pageable = PageRequest.of(articleFilter.getPage(), articleFilter.getSize(), Sort.Direction.DESC,
				"dateArticle");
		Page<Article> pageArticle = repository
				.findAll(Specification.where(ArticleSpecifications.getByArticleID(articleFilter.getArticleId()))
						.and(ArticleSpecifications.getByDateDebut(articleFilter.getDateDebut()))
						.and(ArticleSpecifications.getByDateFin(articleFilter.getDateFin()))
						.and(ArticleSpecifications.getByDescription(articleFilter.getDescription()))
						.and(ArticleSpecifications.getByVisble(articleFilter.getIsVisible()))
						.and(ArticleSpecifications.getByNameUser(articleFilter.getNomUser()))
						.and(ArticleSpecifications.getByIDUser(articleFilter.getIdUser()))
						.and(ArticleSpecifications.getByEmailUser(articleFilter.getEmailUser())), pageable);

		pageArticle.forEach(result::add);

		List<ArticleDto> listArticleDto = mapper.mapToListOfDTO(result);

		return new PageImpl<>(listArticleDto, pageable, pageArticle.getTotalElements());
	}

	@Transactional
	public ArticleDto visibleArticles(Long id) {
		if (id != null) {
			Article article = repository.findById(id).orElse(null);
			if (article == null) {
				throw new NotFoundException("l'article " + id + " Est inexistant");
			}

			article.setIsVisible(Boolean.TRUE);
			article.setDatePublication(LocalDateTime.now());
			Article savedArticle = repository.saveAndFlush(article);
			String email = savedArticle.getUtilisateur().getEmail();
			String msg = "Bonjour,  \n \n";
			msg += "Merci pour votre contribution, votre article vient d’être publié . \n \n";
			msg += "Cordiales salutations \n";
			msg += "Equipe Innovation";
			emailService.sendSimpleMail(email, "Publication d'article", msg);
			
			msg = "Bonjour,  \n \n";
			msg += "L'administrateur vient de publier une nouvelle publication \n \n";
			msg += "Cordialement, \n";
			msg += "Equipe Innovation";
		//	emailService.broadcast(msg, "Nouvelle publication");
			return mapper.mapToDTO(savedArticle);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification l'article");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la modification l'article");
		}
	}

	@Transactional
	public ArticleDto validereArticle(Long id) {
		if (id != null) {
			Article article = repository.findById(id).orElse(null);
			if (article == null) {
				throw new NotFoundException("L'article" + id + " Est inexistant");
			}

			article.setIsVisible(true);
			Article savedArticle = repository.saveAndFlush(article);
			String email = savedArticle.getUtilisateur().getEmail();
			String msg = "Bonjour,  \n \n";
			msg += "Merci pour votre contribution, votre article vient d’être publié . \n \n";
			msg += "Cordiales salutations \n";
			msg += "Equipe Innovation";
			emailService.sendSimpleMail(email, "Publication d'article", msg);
			
			msg = "Bonjour,  \n \n";
			msg += "L'administrateur vient de publier une nouvelle publication \n \n";
			msg += "Cordialement, \n";
			msg += "Equipe Innovation";
			emailService.broadcast(msg, "Nouvelle publication");
			return mapper.mapToDTO(savedArticle);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification d'un article");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la modification d'un article");
		}
	}

	@Transactional
	public void deleteArticle(Long id) {
		if (id != null) {
			Article article = repository.findById(id).orElse(null);
			if (article == null) {
				throw new NotFoundException("L'article " + id + " est inexistant");
			}
			List<ActionLike> actionLikes = likeRepository.findByArticleArticleId(id);
			List<Commentaire> commentaires = commentaireRepository.findByArticleArticleId(id);
			likeRepository.deleteInBatch(actionLikes);
			commentaireRepository.deleteInBatch(commentaires);
			repository.deleteById(id);
		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la suppression d'un article");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la suppression d'un article");
		}
	}

	@Transactional(readOnly = true)
	public List<ArticleDto> getByUser(Long id) {
		if (null != id) {

			log.info("L'affichage des idee a travers son user");
			Utilisateur user = userRepository.findById(id).orElse(null);
			if (user == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}
			List<Article> articles = repository.findByUtilisateurUtilisateurId(id);
			return mapper.mapToListOfDTO(articles);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des idee");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional
	public ArticleDto saveArticle(ArticleDto articleDto) {
		if (articleDto != null) {
			log.info("La Creation d'un article ");

			userService.getUtilisateurById(articleDto.getUtilisateurId());
			Article article = mapper.mapToEntity(articleDto);

			Article savedArticle = repository.save(article);
			Set<String> roles = SecurityContextUtils.getUserRoles();

			// if (roles.contains("ROLE_ADMIN")) {
			// String msg = "Bonjour, \n \n";
			// msg += "L'administrateur vien de publier un nouveau article \n
			// \n";
			// msg += "Cordialement, \n";
			// msg += "Equipe Innovation";
			// emailService.broadcast(msg, "Nouveau article");
			// }
			return mapper.mapToDTO(savedArticle);

		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de la creation d'une article");
			throw new IllegalArgumentException(
					"Une Erreur Fonctionnelle s'est produite lors de la creation d'une article");
		}
	}

	@Transactional
	public ArticleDto updateArticle(ArticleDto articleDto, Long id) {
		if (articleDto != null && id != null) {
			Article article = repository.findById(id).orElse(null);
			if (article == null) {
				throw new NotFoundException("l'article " + id + " Est inexistant");
			}
			userService.getUtilisateurById(articleDto.getUtilisateurId());

			articleDto.setArticleId(id);
			Article articleUpdate = mapper.mapToEntity(articleDto);
			Article savedArticle = repository.saveAndFlush(articleUpdate);
			return mapper.mapToDTO(savedArticle);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification d'un article");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la modification d'un article");
		}
	}

	@Transactional(readOnly = true)
	public ArticleDto getArticleById(Long id) {
		if (null != id) {

			log.info("L'affichage d'une article a travers son Identifiant");
			Article article = repository.findById(id).orElse(null);
			if (article == null) {
				throw new NotFoundException("l'article " + id + " Est inexistant");
			}
			return mapper.mapToDTO(article);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage d'une article");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional(readOnly = true)
	public Page<ArticleDto> getArticlePagable(int page, int size) {
		List<Article> articles = repository.findAll();

		List<ArticleDto> listArticleDto = mapper.mapToListOfDTO(articles);

		int start = (int) PageRequest.of(page, size).getOffset();
		int end = (start + PageRequest.of(page, size).getPageSize()) > listArticleDto.size() ? listArticleDto.size()
				: (start + PageRequest.of(page, size).getPageSize());

		return new PageImpl<>(listArticleDto.subList(start, end), PageRequest.of(page, size), listArticleDto.size());

	}

}
