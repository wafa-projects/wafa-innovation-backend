package com.wafa.api.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wafa.api.dto.EnqueteDto;
import com.wafa.api.dto.QuestionnaireDto;
import com.wafa.api.dto.ReponseEnqueteDto;
import com.wafa.api.entity.ReponseEnquete;
import com.wafa.api.mapper.ReponseEnqueteMapper;
import com.wafa.api.repository.ReponseEnqueteRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EnqueteExcel {

	@Autowired
	private EnqueteService enqueteService;

	@Autowired
	private ReponseEnqueteRepository reponseRepository;

	@Autowired
	private ReponseEnqueteMapper reponseEnqueteMapper;

	private int cpt = 0;

	public void createTitles(Sheet sheet, Workbook workbook) {

		Row row = sheet.createRow(cpt); // row 1
		CellStyle headerStyle = workbook.createCellStyle();

		headerStyle.setBorderBottom(BorderStyle.THIN);
		headerStyle.setBorderTop(BorderStyle.THIN);
		headerStyle.setBorderLeft(BorderStyle.THIN);
		headerStyle.setBorderRight(BorderStyle.THIN);

		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		XSSFFont font = ((XSSFWorkbook) workbook).createFont();
		headerStyle.setFont(font);

		Cell cell = row.createCell(1); // cell A1
		headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		cell.setCellValue("Nom");
		cell.setCellStyle(headerStyle);
		// set vertical alignment center
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);

		cell = row.createCell(2); // cell B1
		cell.setCellValue("prénom");
		// set horizontal alignment center
		cell.setCellStyle(headerStyle);
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);

		cell = row.createCell(3); // cell B1
		cell.setCellValue("matricule");
		// set horizontal alignment center
		cell.setCellStyle(headerStyle);
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);

		cell = row.createCell(4); // cell B1
		cell.setCellValue("entité");
		cell.setCellStyle(headerStyle);
		// set horizontal alignment center
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);

		cell = row.createCell(5); // cell B1
		cell.setCellValue("ville");
		cell.setCellStyle(headerStyle);
		// set horizontal alignment center
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);

		cell = row.createCell(6); // cell B1
		cell.setCellValue("Région");
		cell.setCellStyle(headerStyle);
		// set horizontal alignment center
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);

		cell = row.createCell(0); // cell B1
		cell.setCellValue("Réponse");
		cell.setCellStyle(headerStyle);
		// set horizontal alignment center
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
		cpt++;

	}

	public void createFirstTitles(Sheet sheet, Workbook workbook, String enquete) {

		Row row = sheet.createRow(cpt); // row 1
		CellStyle headerStyle = workbook.createCellStyle();

		headerStyle.setBorderBottom(BorderStyle.THIN);
		headerStyle.setBorderTop(BorderStyle.THIN);
		headerStyle.setBorderLeft(BorderStyle.THIN);
		headerStyle.setBorderRight(BorderStyle.THIN);

		CellRangeAddress mergedRegion = new CellRangeAddress(cpt, cpt, 1, 6);
		sheet.addMergedRegion(mergedRegion);
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		XSSFFont font = ((XSSFWorkbook) workbook).createFont();
		headerStyle.setFont(font);

		Cell cell = row.createCell(0); // cell A1
		headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		font.setColor(IndexedColors.WHITE.getIndex());
		cell.setCellValue("Enquête");

		cell.setCellStyle(headerStyle);
		// set vertical alignment center
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);

		cell = row.createCell(1); // cell B1
		cell.setCellValue(enquete);
		// set horizontal alignment center
		// cell.setCellStyle(headerStyle);
		// CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT,
		// HorizontalAlignment.CENTER);
		cpt++;

	}

	public void createSecondeTitles(Sheet sheet, Workbook workbook, String question) {

		Row row = sheet.createRow(cpt); // row 1
		CellStyle headerStyle = workbook.createCellStyle();

		CellRangeAddress mergedRegion = new CellRangeAddress(cpt, cpt, 1, 6);
		sheet.addMergedRegion(mergedRegion);

		headerStyle.setBorderBottom(BorderStyle.THIN);
		headerStyle.setBorderTop(BorderStyle.THIN);
		headerStyle.setBorderLeft(BorderStyle.THIN);
		headerStyle.setBorderRight(BorderStyle.THIN);

		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		XSSFFont font = ((XSSFWorkbook) workbook).createFont();
		headerStyle.setFont(font);

		Cell cell = row.createCell(0); // cell A1
		headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		cell.setCellValue("Question");
		cell.setCellStyle(headerStyle);
		// set vertical alignment center
		CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);

		cell = row.createCell(1); // cell B1
		cell.setCellValue(question);
		// set horizontal alignment center
		// cell.setCellStyle(headerStyle);
		// CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT,
		// HorizontalAlignment.CENTER);
		cpt++;

	}

	public void createListFields(Sheet sheet, Workbook workbook, List<ReponseEnqueteDto> reponseEnqueteDtos) {

		for (ReponseEnqueteDto reponseEnqueteDto : reponseEnqueteDtos) {
			Row row = sheet.createRow(cpt); // row 1
			CellStyle headerStyle = workbook.createCellStyle();
			headerStyle.setBorderBottom(BorderStyle.THIN);
			headerStyle.setBorderTop(BorderStyle.THIN);
			headerStyle.setBorderLeft(BorderStyle.THIN);
			headerStyle.setBorderRight(BorderStyle.THIN);

			Cell cell = row.createCell(1); // cell A1

			cell.setCellValue(reponseEnqueteDto.getUtilisateur().getNom());
			CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
			cell.setCellStyle(headerStyle);

			cell = row.createCell(2); // cell B1
			cell.setCellValue(reponseEnqueteDto.getUtilisateur().getPrenom());
			// set horizontal alignment center
			CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
			cell.setCellStyle(headerStyle);

			cell = row.createCell(3); // cell B1
			cell.setCellValue(reponseEnqueteDto.getUtilisateur().getMatricule());
			// set horizontal alignment center
			CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
			cell.setCellStyle(headerStyle);

			cell = row.createCell(4); // cell B1
			cell.setCellValue(reponseEnqueteDto.getUtilisateur().getEntitee());
			// set horizontal alignment center
			CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
			cell.setCellStyle(headerStyle);

			cell = row.createCell(5); // cell B1
			cell.setCellValue(reponseEnqueteDto.getUtilisateur().getVille());
			// set horizontal alignment center
			CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
			cell.setCellStyle(headerStyle);

			cell = row.createCell(6); // cell B1
			cell.setCellValue(reponseEnqueteDto.getUtilisateur().getRegion());
			// set horizontal alignment center
			CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
			cell.setCellStyle(headerStyle);

			cell = row.createCell(0); // cell B1
			cell.setCellValue(reponseEnqueteDto.getReponse());
			// set horizontal alignment center
			CellUtil.setCellStyleProperty(cell, CellUtil.ALIGNMENT, HorizontalAlignment.CENTER);
			cell.setCellStyle(headerStyle);
			cpt++;
		}

	}

	public ByteArrayInputStream writeExcel(Long id) throws IOException {
		log.info("generate enquetes excel");
		Workbook workbook = new XSSFWorkbook();
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {
			
//			Sheet sheet = workbook.createSheet("Enquêtes");

			EnqueteDto enqueteDto = enqueteService.getEnqueteById(id);
			

			cpt = 0;
			if (enqueteDto != null) {

				List<QuestionnaireDto> questionnaireDtos = enqueteDto.getQuestionnaires();

				if (questionnaireDtos != null && !questionnaireDtos.isEmpty()) {
					int nbr=0;

					for (QuestionnaireDto questionnaireDto : questionnaireDtos) {
						cpt=0;
						Sheet sheet = workbook.createSheet("Question "+nbr);
						for (int i = 0; i <= 7; i++) {
							sheet.setColumnWidth(i, 9000);
						}
						createFirstTitles(sheet, workbook, enqueteDto.getDescription());
						createSecondeTitles(sheet, workbook, questionnaireDto.getDescription());

						List<ReponseEnquete> reponses = reponseRepository
								.findByQuestionnaireQuestionnaireId(questionnaireDto.getQuestionnaireId());
						List<ReponseEnqueteDto> listReponseDto = reponseEnqueteMapper.mapToListOfDTO(reponses);
						if (listReponseDto != null && !listReponseDto.isEmpty()) {
							createTitles(sheet, workbook);
							createListFields(sheet, workbook, listReponseDto);
						}
						nbr++;
					}
				}
			}

			workbook.write(out);
		} finally {
			if (workbook != null) {

				workbook.close();

			}
		}
		return new ByteArrayInputStream(out.toByteArray());
	}

}
