//package com.wafa.api.service;
//
//import java.net.URI;
//import java.util.Arrays;
//import java.util.List;
//
//import org.keycloak.KeycloakPrincipal;
//import org.keycloak.KeycloakSecurityContext;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.core.ParameterizedTypeReference;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.MediaType;
//import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;
//
//import com.wafa.api.dto.KeycloakUser;
//import com.wafa.api.exception.NotFoundException;
//
//@Service
//public class KeycloakUserService {
//
//	@Value("${keycloak.auth-server-url}")
//	private String keycloakServerUrl;
//
//	@Value("${keycloak.realm}")
//	private String keycloakRealm;
//
//	
//
//	public List<KeycloakUser> getCustomerByUsername(String username,
//			KeycloakPrincipal<KeycloakSecurityContext> principal) throws NotFoundException {
//		KeycloakSecurityContext context = principal.getKeycloakSecurityContext();
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_JSON);
//		headers.set("Authorization", "Bearer " + context.getTokenString());
//
//		StringBuilder sb = new StringBuilder(keycloakServerUrl);
//		sb.append("/admin/realms/").append(keycloakRealm).append("/users");
//		sb.append("?username=").append(username);
//
//		HttpEntity<String> entity = new HttpEntity<String>(headers);
//		RestTemplate restTemplate = new RestTemplate();
//		KeycloakUser[] users = restTemplate
//				.exchange(URI.create(sb.toString()), HttpMethod.GET, entity, KeycloakUser[].class).getBody();
//		if (users.length == 0) {
//			throw new NotFoundException(username);
//		}
//
//		return Arrays.asList(Arrays.stream(users).toArray(KeycloakUser[]::new));
//	}
//
//	public List<KeycloakUser> getCustomers(KeycloakPrincipal<KeycloakSecurityContext> principal) {
//		KeycloakSecurityContext context = principal.getKeycloakSecurityContext();
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_JSON);
//		headers.set("Authorization", "Bearer " + context.getTokenString());
//
//		StringBuilder sb = new StringBuilder(keycloakServerUrl);
//		sb.append("/admin/realms/").append(keycloakRealm).append("/users");
//
//		HttpEntity<String> entity = new HttpEntity<String>(headers);
//		RestTemplate restTemplate = new RestTemplate();
//		List<KeycloakUser> users = restTemplate.exchange(URI.create(sb.toString()), HttpMethod.GET, entity,
//				new ParameterizedTypeReference<List<KeycloakUser>>() {
//				}).getBody();
//
//		return users;
//	}
//
//
//}
