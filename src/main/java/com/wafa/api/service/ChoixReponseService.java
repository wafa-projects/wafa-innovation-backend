package com.wafa.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wafa.api.dto.ChoixReponseDto;
import com.wafa.api.entity.ChoixReponse;
import com.wafa.api.entity.Questionnaire;
import com.wafa.api.exception.NotFoundException;
import com.wafa.api.mapper.ChoixReponseMapper;
import com.wafa.api.repository.ChoixReponseRepository;
import com.wafa.api.repository.QuestionnaireRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ChoixReponseService {

	@Autowired
	private ChoixReponseRepository repository;
	
	@Autowired
	private QuestionnaireRepository questionnaireRepository;
	
	@Autowired
	private ChoixReponseMapper mapper;

	@Transactional(readOnly = true)
	public List<ChoixReponseDto> getAllChoixReponse() {

		log.info("L'affichage des ChoixReponses");
		List<ChoixReponse> choixReponses = repository.findAll();
		return mapper.mapToListOfDTO(choixReponses);
	}
	
	@Transactional
	public void deleteChoix(Long id){
		if(id!=null){
			ChoixReponse choixReponse = repository.findById(id).orElse(null);
			if (choixReponse == null) {
				throw new NotFoundException("Le choix " + id + " est inexistant");
			}
			repository.deleteById(id);
		}
		else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la suppression d'un choix");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la suppression d'un choix");
		}
	}

	@Transactional(readOnly = true)
	public List<ChoixReponseDto> getByQuestionnaire(Long id) {
		if (null != id) {

			log.info("L'affichage des choix a travers son questionnaire");
			Questionnaire user = questionnaireRepository.findById(id).orElse(null);
			if (user==null) {
				throw new NotFoundException("questionnaire " + id + " Est inexistant");
			}
			List<ChoixReponse> choixReponses = repository.findByQuestionnaireQuestionnaireId(id);
			return mapper.mapToListOfDTO(choixReponses);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des idee");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}


	@Transactional
	public ChoixReponseDto saveChoixReponse(ChoixReponseDto choixReponseDto) {
		if (choixReponseDto != null) {
			log.info("La Creation d'un choixReponse ");
			
			
				ChoixReponse choixReponse = mapper.mapToEntity(choixReponseDto);

				ChoixReponse savedChoixReponse = repository.save(choixReponse);
				return mapper.mapToDTO(savedChoixReponse);
			
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de la creation d'une choixReponse");
			throw new IllegalArgumentException("Une Erreur Fonctionnelle s'est produite lors de la creation d'une choixReponse");
		}
	}

	@Transactional
	public ChoixReponseDto updateChoixReponse(ChoixReponseDto choixReponseDto, Long id) {
		if (choixReponseDto != null && id != null) {
			ChoixReponse choixReponse = repository.findById(id).orElse(null);
			if (choixReponse == null) {
				throw new NotFoundException("l'choixReponse " + id + " Est inexistant");
			}
			
			choixReponseDto.setChoixId(id);
			ChoixReponse choixReponseUpdate=mapper.mapToEntity(choixReponseDto);
			ChoixReponse savedChoixReponse = repository.saveAndFlush(choixReponseUpdate);
			return mapper.mapToDTO(savedChoixReponse);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification d'un choixReponse");
			throw new IllegalArgumentException("Une Erreur fonctionnelle s'est produite lors de la modification d'un choixReponse");
		}
	}



	@Transactional(readOnly = true)
	public ChoixReponseDto getChoixReponseById(Long id) {
		if (null != id) {

			log.info("L'affichage d'une choixReponse a travers son Identifiant");
			ChoixReponse choixReponse = repository.findById(id).orElse(null);
			if (choixReponse == null) {
				throw new NotFoundException("l'choixReponse " + id + " Est inexistant");
			}
			return mapper.mapToDTO(choixReponse);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage d'une choixReponse");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	

	@Transactional(readOnly = true)
	public Page<ChoixReponseDto> getChoixReponsePagable(int page, int size) {
		List<ChoixReponse> choixReponses = repository.findAll();

		List<ChoixReponseDto> listChoixReponseDto = mapper.mapToListOfDTO(choixReponses);

		int start =  (int) PageRequest.of(page, size).getOffset();
		int end = (start + PageRequest.of(page, size).getPageSize()) > listChoixReponseDto.size() ? listChoixReponseDto.size()
				: (start + PageRequest.of(page, size).getPageSize());
		
		return new PageImpl<>(listChoixReponseDto.subList(start, end), PageRequest.of(page, size),
				listChoixReponseDto.size());

	}
	
}
