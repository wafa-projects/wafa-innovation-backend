package com.wafa.api.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wafa.api.dto.AcceuilDto;
import com.wafa.api.entity.AcceuilPage;
import com.wafa.api.mapper.AcceuilMapper;
import com.wafa.api.repository.AcceuilPageRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AcceuilPageService {

	@Autowired
	private AcceuilPageRepository repository;
	
	@Autowired
	private AcceuilMapper mapper;

	@Transactional
	public AcceuilDto getAcceuil() {
		List<AcceuilPage> acceuilPages = repository.findAll();
		AcceuilDto acceuilDto=null;
		if(!acceuilPages.isEmpty()){
			acceuilDto=mapper.mapToDTO(acceuilPages.get(0));
		}
		
		return acceuilDto;
	}
	
	@Transactional
	public AcceuilDto saveAcceuil(AcceuilDto acceuilPage) {
		if (acceuilPage != null) {
			log.info("La Creation d'acceuil ");

			return mapper.mapToDTO(repository.save(mapper.mapToEntity(acceuilPage)));

		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de la creation d'acceuil");
			throw new IllegalArgumentException("Une Erreur Fonctionnelle s'est produite lors de la creation d'acceuil");
		}

	}

	@Transactional
	public AcceuilDto updateAcceuil(AcceuilDto acceuilPage) {

		if (acceuilPage != null) {
			log.info("La Creation d'acceuil ");

			AcceuilDto acPage = getAcceuil();
			
			acceuilPage.setAcceuilId(acPage.getAcceuilId());
			
			AcceuilPage acceuilPageSave=repository.saveAndFlush(mapper.mapToEntity(acceuilPage));
			
			return mapper.mapToDTO(acceuilPageSave);

		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de la creation d'acceuil");
			throw new IllegalArgumentException("Une Erreur Fonctionnelle s'est produite lors de la creation d'acceuil");
		}

	}
}
