package com.wafa.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wafa.api.dto.LikeDto;
import com.wafa.api.entity.ActionLike;
import com.wafa.api.entity.Article;
import com.wafa.api.exception.NotFoundException;
import com.wafa.api.mapper.LikeMapper;
import com.wafa.api.repository.ArticleRepository;
import com.wafa.api.repository.LikeRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class LikeService {

	@Autowired
	private LikeRepository repository;

	@Autowired
	private ArticleRepository articleRepository;

	@Autowired
	private LikeMapper mapper;
	
	@Autowired
	private UtilisateurService userService;

	@Transactional
	public LikeDto saveLike(LikeDto likeDto) {
		if (likeDto != null) {
			log.info("La Creation d'un like ");
			userService.getUtilisateurById(likeDto.getUtilisateurId());
			ActionLike like = mapper.mapToEntity(likeDto);
			ActionLike userLike = repository.findByArticleArticleIdAndUtilisateurUtilisateurId(likeDto.getArticleId(),
					likeDto.getUtilisateurId());
			if (userLike == null) {
				ActionLike savedLike = repository.save(like);
				return mapper.mapToDTO(savedLike);
			} else {
				log.info("Vous avez déja aimé cet article");
				throw new IllegalArgumentException("Vous avez déja aimé cet article");
			}

		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de la creation d'une like");
			throw new IllegalArgumentException(
					"Une Erreur Fonctionnelle s'est produite lors de la creation d'une like");
		}
	}

	@Transactional
	public LikeDto updateLike(LikeDto likeDto, Long id) {
		if (likeDto != null && id != null) {
			ActionLike like = repository.findById(id).orElse(null);
			if (like == null) {
				throw new NotFoundException("l'like " + id + " Est inexistant");
			}
			userService.getUtilisateurById(likeDto.getUtilisateurId());
			likeDto.setLikeId(id);
			ActionLike likeUpdate = mapper.mapToEntity(likeDto);
			ActionLike savedLike = repository.saveAndFlush(likeUpdate);
			return mapper.mapToDTO(savedLike);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification d'un like");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la modification d'un like");
		}
	}

	@Transactional(readOnly = true)
	public LikeDto getLikeById(Long id) {
		if (null != id) {

			log.info("L'affichage d'une like a travers son Identifiant");
			ActionLike like = repository.findById(id).orElse(null);
			if (like == null) {
				throw new NotFoundException("l'like " + id + " Est inexistant");
			}
			return mapper.mapToDTO(like);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage d'une like");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional(readOnly = true)
	public List<LikeDto> getLikes() {

		return mapper.mapToListOfDTO(repository.findAll());

	}

	@Transactional(readOnly = true)
	public long nbrLikes(Long id) {

		if (null != id) {

			log.info("nbr like");
			Article article = articleRepository.findById(id).orElse(null);
			if (article == null) {
				throw new NotFoundException("l'article " + id + " Est inexistant");
			}
			long likes = repository.countByArticleArticleId(id);

			return likes;
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des question");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}

	}

	@Transactional(readOnly = true)
	public Page<LikeDto> getByArticlePagable(int page, int size, Long id) {

		if (null != id) {

			log.info("L'affichage des question a travers son parent");
			Article article = articleRepository.findById(id).orElse(null);
			if (article == null) {
				throw new NotFoundException("l'article " + id + " Est inexistant");
			}
			List<ActionLike> likes = repository.findByArticleArticleId(id);
			List<LikeDto> listLikeDto = mapper.mapToListOfDTO(likes);

			int start = (int) PageRequest.of(page, size).getOffset();
			int end = (start + PageRequest.of(page, size).getPageSize()) > listLikeDto.size() ? listLikeDto.size()
					: (start + PageRequest.of(page, size).getPageSize());

			return new PageImpl<>(listLikeDto.subList(start, end), PageRequest.of(page, size), listLikeDto.size());
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des question");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}

	}

}
