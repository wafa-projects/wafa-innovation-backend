package com.wafa.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wafa.api.dto.QuestionDto;
import com.wafa.api.dto.UtilisateurDto;
import com.wafa.api.entity.Question;
import com.wafa.api.entity.Utilisateur;
import com.wafa.api.exception.NotFoundException;
import com.wafa.api.mapper.QuestionMapper;
import com.wafa.api.repository.QuestionRepository;
import com.wafa.api.repository.UtilisateurRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class QuestionService {

	@Autowired
	private EmailService emailService;

	@Autowired
	private QuestionRepository repository;

	@Autowired
	private QuestionMapper mapper;

	@Autowired
	private UtilisateurRepository userRepository;

	@Autowired
	private UtilisateurService userService;
	
	@Value("${email.admin.adress}")
	private String email;

	@Transactional(readOnly = true)
	public List<QuestionDto> getAllQuestion() {

		log.info("L'affichage des Questions");
		List<Question> questions = repository.findAll();
		return mapper.mapToListOfDTO(questions);
	}

	@Transactional
	public void deleteQuestion(Long id) {
		if (id != null) {
			Question question = repository.findById(id).orElse(null);
			if (question == null) {
				throw new NotFoundException("La question " + id + " est inexistant");
			}
			repository.deleteById(id);
		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la suppression d'une question");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la suppression d'une question");
		}
	}

	@Transactional(readOnly = true)
	public List<QuestionDto> getByUser(Long id) {
		if (null != id) {

			log.info("L'affichage des idee a travers son user");
			Utilisateur user = userRepository.findById(id).orElse(null);
			if (user == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}
			List<Question> questions = repository.findByUtilisateurUtilisateurId(id);
			return mapper.mapToListOfDTO(questions);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des idee");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional(readOnly = true)
	public long nbrQuestion(Long id) {

		if (null != id) {

			log.info("nbr idee");
			Utilisateur utilisateur = userRepository.findById(id).orElse(null);
			if (utilisateur == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}
			long questions = repository.countByUtilisateurUtilisateurId(id);

			return questions;
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des questions");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}

	}

	@Transactional(readOnly = true)
	public List<QuestionDto> getByQuestionParent(Long id) {
		if (null != id) {

			log.info("L'affichage des question a travers son parent");
			Question question = repository.findById(id).orElse(null);
			if (question == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}
			List<Question> questions = repository.findByQuestionParentQuestionId(id);
			return mapper.mapToListOfDTO(questions);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des question");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional
	public QuestionDto saveQuestion(QuestionDto questionDto) {
		if (questionDto != null) {
			log.info("La Creation d'un question ");
			
			UtilisateurDto user=userService.getUtilisateurById(questionDto.getUtilisateurId());

			Question question = mapper.mapToEntity(questionDto);

			Question savedQuestion = repository.save(question);
			String msg = "Bonjour,  \n \n";
			msg += "Vous avez recu une question de la part du " + user.getPrenom() + " "
					+ user.getNom() + " : \n";
			msg +=savedQuestion.getDescription()+" \n \n";
			msg += "Cordialement, \n";
			msg += "Equipe Innovation";
			emailService.sendSimpleMail(email, "Question", msg);

			return mapper.mapToDTO(savedQuestion);

		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de la creation d'une question");
			throw new IllegalArgumentException(
					"Une Erreur Fonctionnelle s'est produite lors de la creation d'une question");
		}
	}

	@Transactional
	public QuestionDto visibleQuestion(Long id) {
		if (id != null) {
			Question question = repository.findById(id).orElse(null);
			if (question == null) {
				throw new NotFoundException("la question " + id + " Est inexistant");
			}

			question.setIsVisible(Boolean.TRUE);
			Question savedQuestion = repository.saveAndFlush(question);
			return mapper.mapToDTO(savedQuestion);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification d'un question");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la modification d'un question");
		}
	}

	@Transactional
	public QuestionDto updateQuestion(QuestionDto questionDto, Long id) {
		if (questionDto != null && id != null) {
			Question question = repository.findById(id).orElse(null);
			if (question == null) {
				throw new NotFoundException("l'question " + id + " Est inexistant");
			}
			userService.getUtilisateurById(questionDto.getUtilisateurId());

			questionDto.setQuestionId(id);
			Question questionUpdate = mapper.mapToEntity(questionDto);
			Question savedQuestion = repository.saveAndFlush(questionUpdate);
			return mapper.mapToDTO(savedQuestion);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification d'un question");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la modification d'un question");
		}
	}

	@Transactional(readOnly = true)
	public QuestionDto getQuestionById(Long id) {
		if (null != id) {

			log.info("L'affichage d'une question a travers son Identifiant");
			Question question = repository.findById(id).orElse(null);
			if (question == null) {
				throw new NotFoundException("l'question " + id + " Est inexistant");
			}
			return mapper.mapToDTO(question);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage d'une question");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional(readOnly = true)
	public Page<QuestionDto> getQuestionPagable(int page, int size) {
		List<Question> questions = repository.findAll();

		List<QuestionDto> listQuestionDto = mapper.mapToListOfDTO(questions);

		int start = (int) PageRequest.of(page, size).getOffset();
		int end = (start + PageRequest.of(page, size).getPageSize()) > listQuestionDto.size() ? listQuestionDto.size()
				: (start + PageRequest.of(page, size).getPageSize());

		return new PageImpl<>(listQuestionDto.subList(start, end), PageRequest.of(page, size), listQuestionDto.size());

	}

	@Transactional(readOnly = true)
	public Page<QuestionDto> getByUserPage(int page, int size, Long id) {
		if (null != id) {

			log.info("L'affichage des questions a travers son user");
			Utilisateur user = userRepository.findById(id).orElse(null);
			if (user == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}
			List<Question> questions = repository.findByUtilisateurUtilisateurId(id);
			List<QuestionDto> listQuestionDto = mapper.mapToListOfDTO(questions);

			int start = (int) PageRequest.of(page, size).getOffset();
			int end = (start + PageRequest.of(page, size).getPageSize()) > listQuestionDto.size()
					? listQuestionDto.size() : (start + PageRequest.of(page, size).getPageSize());

			return new PageImpl<>(listQuestionDto.subList(start, end), PageRequest.of(page, size),
					listQuestionDto.size());
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des idee");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

}
