package com.wafa.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wafa.api.dto.ReponseDto;
import com.wafa.api.entity.Question;
import com.wafa.api.entity.Reponse;
import com.wafa.api.entity.Utilisateur;
import com.wafa.api.exception.NotFoundException;
import com.wafa.api.mapper.ReponseMapper;
import com.wafa.api.repository.QuestionRepository;
import com.wafa.api.repository.ReponseRepository;
import com.wafa.api.repository.UtilisateurRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ReponseService {

	@Autowired
	private ReponseRepository repository;

	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@Autowired
	private QuestionRepository questionRepository;

	@Autowired
	private ReponseMapper mapper;
	
	@Autowired
	private UtilisateurService userService;

	@Transactional(readOnly = true)
	public List<ReponseDto> getAllReponse() {

		log.info("L'affichage des Reponses");
		List<Reponse> reponses = repository.findAll();
		return mapper.mapToListOfDTO(reponses);
	}

	@Transactional(readOnly = true)
	public List<ReponseDto> getByUser(Long id) {
		if (null != id) {

			log.info("L'affichage des idee a travers son user");
			Utilisateur user = utilisateurRepository.findById(id).orElse(null);
			if (user == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}
			List<Reponse> reponses = repository.findByUtilisateurUtilisateurId(id);
			return mapper.mapToListOfDTO(reponses);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des idee");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}
	
	@Transactional
	public void deleteReponse(Long id){
		if(id!=null){
			Reponse reponse = repository.findById(id).orElse(null);
			if (reponse == null) {
				throw new NotFoundException("La reponse " + id + " est inexistant");
			}
			repository.deleteById(id);
		}
		else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la suppression d'une reponse");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la suppression d'une reponse");
		}
	}

	
	@Transactional
	public ReponseDto saveReponse(ReponseDto reponseDto) {
		if (reponseDto != null) {
			log.info("La Creation d'un reponse ");

			Reponse reponse = mapper.mapToEntity(reponseDto);
			userService.getUtilisateurById(reponseDto.getUtilisateurId());

			Reponse savedReponse = repository.save(reponse);
			return mapper.mapToDTO(savedReponse);

		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de la creation d'une reponse");
			throw new IllegalArgumentException(
					"Une Erreur Fonctionnelle s'est produite lors de la creation d'une reponse");
		}
	}

	@Transactional
	public ReponseDto updateReponse(ReponseDto reponseDto, Long id) {
		if (reponseDto != null && id != null) {
			Reponse reponse = repository.findById(id).orElse(null);
			if (reponse == null) {
				throw new NotFoundException("l'reponse " + id + " Est inexistant");
			}
			userService.getUtilisateurById(reponseDto.getUtilisateurId());

			reponseDto.setReponseId(id);
			Reponse reponseUpdate = mapper.mapToEntity(reponseDto);
			Reponse savedReponse = repository.saveAndFlush(reponseUpdate);
			return mapper.mapToDTO(savedReponse);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification d'un reponse");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la modification d'un reponse");
		}
	}

	@Transactional(readOnly = true)
	public ReponseDto getReponseById(Long id) {
		if (null != id) {

			log.info("L'affichage d'une reponse a travers son Identifiant");
			Reponse reponse = repository.findById(id).orElse(null);
			if (reponse == null) {
				throw new NotFoundException("l'reponse " + id + " Est inexistant");
			}
			return mapper.mapToDTO(reponse);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage d'une reponse");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional(readOnly = true)
	public Page<ReponseDto> getReponseByQestionPagable(int page, int size, Long id) {
		if (null != id) {

			log.info("L'affichage des question a travers son parent");
			Question question = questionRepository.findById(id).orElse(null);
			if (question == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}
			List<Reponse> reponses = repository.findByQuestionQuestionId(id);
			List<ReponseDto> listReponseDto = mapper.mapToListOfDTO(reponses);

			int start = (int) PageRequest.of(page, size).getOffset();
			int end = (start + PageRequest.of(page, size).getPageSize()) > listReponseDto.size() ? listReponseDto.size()
					: (start + PageRequest.of(page, size).getPageSize());

			return new PageImpl<>(listReponseDto.subList(start, end), PageRequest.of(page, size),
					listReponseDto.size());
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des question");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}

	}

	@Transactional(readOnly = true)
	public List<ReponseDto> getByQuestion(Long id) {
		if (null != id) {

			log.info("L'affichage des question a travers son parent");
			Question question = questionRepository.findById(id).orElse(null);
			if (question == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}
			List<Reponse> reponses = repository.findByQuestionQuestionId(id);
			return mapper.mapToListOfDTO(reponses);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des question");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

}
