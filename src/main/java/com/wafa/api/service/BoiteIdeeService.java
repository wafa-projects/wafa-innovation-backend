package com.wafa.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wafa.api.dto.BoiteIdeeDto;
import com.wafa.api.entity.BoiteIdee;
import com.wafa.api.entity.Utilisateur;
import com.wafa.api.exception.NotFoundException;
import com.wafa.api.mapper.BoiteIdeeMapper;
import com.wafa.api.repository.BoiteIdeeRepository;
import com.wafa.api.repository.UtilisateurRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BoiteIdeeService {

	@Autowired
	private BoiteIdeeRepository repository;

	@Autowired
	private UtilisateurRepository userRepository;

	@Autowired
	private BoiteIdeeMapper mapper;
	
	@Autowired
	private UtilisateurService userService;

	@Transactional(readOnly = true)
	public List<BoiteIdeeDto> getAllBoiteIdee() {

		log.info("L'affichage des BoiteIdees");
		List<BoiteIdee> boiteIdees = repository.findAll();
		return mapper.mapToListOfDTO(boiteIdees);
	}

	@Transactional
	public void deleteBoiteIdee(Long id) {
		if (id != null) {
			BoiteIdee boiteIdee = repository.findById(id).orElse(null);
			if (boiteIdee == null) {
				throw new NotFoundException("L'idée " + id + " est inexistant");
			}
			repository.deleteById(id);
		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la suppression d'une idée");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la suppression d'une idée");
		}
	}

	@Transactional(readOnly = true)
	public long nbrIdee(Long id) {

		if (null != id) {

			log.info("nbr idee");
			Utilisateur utilisateur = userRepository.findById(id).orElse(null);
			if (utilisateur == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}
			long idees = repository.countByUtilisateurUtilisateurId(id);

			return idees;
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des idees");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}

	}

	@Transactional(readOnly = true)
	public List<BoiteIdeeDto> getByUser(Long id) {
		if (null != id) {
			log.info("L'affichage des idee a travers son user");
			Utilisateur user = userRepository.findById(id).orElse(null);
			if (user == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}
			List<BoiteIdee> boiteIdees = repository.findByUtilisateurUtilisateurId(id);
			return mapper.mapToListOfDTO(boiteIdees);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des idee");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional
	public BoiteIdeeDto saveBoiteIdee(BoiteIdeeDto boiteIdeeDto) {
		if (boiteIdeeDto != null) {
			log.info("La Creation d'un boiteIdee ");

			userService.getUtilisateurById(boiteIdeeDto.getUtilisateurId());
			BoiteIdee boiteIdee = mapper.mapToEntity(boiteIdeeDto);

			BoiteIdee savedBoiteIdee = repository.save(boiteIdee);
			return mapper.mapToDTO(savedBoiteIdee);

		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de la creation d'une boiteIdee");
			throw new IllegalArgumentException(
					"Une Erreur Fonctionnelle s'est produite lors de la creation d'une boiteIdee");
		}
	}

	@Transactional
	public BoiteIdeeDto updateBoiteIdee(BoiteIdeeDto boiteIdeeDto, Long id) {
		if (boiteIdeeDto != null && id != null) {
			BoiteIdee boiteIdee = repository.findById(id).orElse(null);
			if (boiteIdee == null) {
				throw new NotFoundException("l'boiteIdee " + id + " Est inexistant");
			}
			userService.getUtilisateurById(boiteIdeeDto.getUtilisateurId());
			boiteIdeeDto.setIdeeId(id);
			BoiteIdee boiteIdeeUpdate = mapper.mapToEntity(boiteIdeeDto);
			BoiteIdee savedBoiteIdee = repository.saveAndFlush(boiteIdeeUpdate);
			return mapper.mapToDTO(savedBoiteIdee);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification d'un boiteIdee");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la modification d'un boiteIdee");
		}
	}

	@Transactional(readOnly = true)
	public BoiteIdeeDto getBoiteIdeeById(Long id) {
		if (null != id) {

			log.info("L'affichage d'une boiteIdee a travers son Identifiant");
			BoiteIdee boiteIdee = repository.findById(id).orElse(null);
			if (boiteIdee == null) {
				throw new NotFoundException("l'boiteIdee " + id + " Est inexistant");
			}
			return mapper.mapToDTO(boiteIdee);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage d'une boiteIdee");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional(readOnly = true)
	public Page<BoiteIdeeDto> getBoiteIdeePagable(int page, int size) {
		List<BoiteIdee> boiteIdees = repository.findAll();

		List<BoiteIdeeDto> listBoiteIdeeDto = mapper.mapToListOfDTO(boiteIdees);

		int start = (int) PageRequest.of(page, size).getOffset();
		int end = (start + PageRequest.of(page, size).getPageSize()) > listBoiteIdeeDto.size() ? listBoiteIdeeDto.size()
				: (start + PageRequest.of(page, size).getPageSize());

		return new PageImpl<>(listBoiteIdeeDto.subList(start, end), PageRequest.of(page, size),
				listBoiteIdeeDto.size());

	}

}
