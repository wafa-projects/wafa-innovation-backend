package com.wafa.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wafa.api.dto.QuestionnaireDto;
import com.wafa.api.entity.Enquete;
import com.wafa.api.entity.Questionnaire;
import com.wafa.api.exception.NotFoundException;
import com.wafa.api.mapper.QuestionnaireMapper;
import com.wafa.api.repository.EnqueteRepository;
import com.wafa.api.repository.QuestionnaireRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class QuestionnaireService {

	@Autowired
	private QuestionnaireRepository repository;
	
	@Autowired
	private EnqueteRepository enqueteRepository;
	
	@Autowired
	private QuestionnaireMapper mapper;

	@Transactional(readOnly = true)
	public List<QuestionnaireDto> getAllQuestionnaire() {

		log.info("L'affichage des Questionnaires");
		List<Questionnaire> questionnaires = repository.findAll();
		return mapper.mapToListOfDTO(questionnaires);
	}

	
	@Transactional
	public void deleteQuestionnaire(Long id){
		if(id!=null){
			Questionnaire questionnaire = repository.findById(id).orElse(null);
			if (questionnaire == null) {
				throw new NotFoundException("Le questionnaire " + id + " est inexistant");
			}
			repository.deleteById(id);
		}
		else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la suppression d'un questionnaire");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la suppression d'un questionnaire");
		}
	}
	
	@Transactional(readOnly = true)
	public List<QuestionnaireDto> getByUser(Long id) {
		if (null != id) {

			log.info("L'affichage des idee a travers son user");
			Enquete enquete = enqueteRepository.findById(id).orElse(null);
			if (enquete==null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}
			List<Questionnaire> enquetes = repository.findByEnqueteEnqueteId(id);
			return mapper.mapToListOfDTO(enquetes);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des idee");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional
	public QuestionnaireDto saveQuestionnaire(QuestionnaireDto questionnaireDto) {
		if (questionnaireDto != null) {
			log.info("La Creation d'un questionnaire ");
			
			
				Questionnaire questionnaire = mapper.mapToEntity(questionnaireDto);

				Questionnaire savedQuestionnaire = repository.save(questionnaire);
				return mapper.mapToDTO(savedQuestionnaire);
			
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de la creation d'une questionnaire");
			throw new IllegalArgumentException("Une Erreur Fonctionnelle s'est produite lors de la creation d'une questionnaire");
		}
	}

	@Transactional
	public QuestionnaireDto updateQuestionnaire(QuestionnaireDto questionnaireDto, Long id) {
		if (questionnaireDto != null && id != null) {
			Questionnaire questionnaire = repository.findById(id).orElse(null);
			if (questionnaire == null) {
				throw new NotFoundException("l'questionnaire " + id + " Est inexistant");
			}
			
			questionnaireDto.setQuestionnaireId(id);
			Questionnaire questionnaireUpdate=mapper.mapToEntity(questionnaireDto);
			Questionnaire savedQuestionnaire = repository.saveAndFlush(questionnaireUpdate);
			return mapper.mapToDTO(savedQuestionnaire);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification d'un questionnaire");
			throw new IllegalArgumentException("Une Erreur fonctionnelle s'est produite lors de la modification d'un questionnaire");
		}
	}



	@Transactional(readOnly = true)
	public QuestionnaireDto getQuestionnaireById(Long id) {
		if (null != id) {

			log.info("L'affichage d'une questionnaire a travers son Identifiant");
			Questionnaire questionnaire = repository.findById(id).orElse(null);
			if (questionnaire == null) {
				throw new NotFoundException("l'questionnaire " + id + " Est inexistant");
			}
			return mapper.mapToDTO(questionnaire);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage d'une questionnaire");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	

	@Transactional(readOnly = true)
	public Page<QuestionnaireDto> getQuestionnairePagable(int page, int size) {
		List<Questionnaire> questionnaires = repository.findAll();

		List<QuestionnaireDto> listQuestionnaireDto = mapper.mapToListOfDTO(questionnaires);

		int start =  (int) PageRequest.of(page, size).getOffset();
		int end = (start + PageRequest.of(page, size).getPageSize()) > listQuestionnaireDto.size() ? listQuestionnaireDto.size()
				: (start + PageRequest.of(page, size).getPageSize());
		
		return new PageImpl<>(listQuestionnaireDto.subList(start, end), PageRequest.of(page, size),
				listQuestionnaireDto.size());

	}
	
}
