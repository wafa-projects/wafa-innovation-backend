package com.wafa.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wafa.api.dto.EnqueteDto;
import com.wafa.api.dto.QuestionnaireDto;
import com.wafa.api.dto.ReponseEnqueteDto;
import com.wafa.api.entity.ChoixReponse;
import com.wafa.api.entity.Enquete;
import com.wafa.api.entity.Questionnaire;
import com.wafa.api.entity.ReponseEnquete;
import com.wafa.api.entity.Utilisateur;
import com.wafa.api.exception.NotFoundException;
import com.wafa.api.mapper.EnqueteMapper;
import com.wafa.api.mapper.QuestionnaireMapper;
import com.wafa.api.mapper.ReponseEnqueteMapper;
import com.wafa.api.repository.EnqueteRepository;
import com.wafa.api.repository.ReponseEnqueteRepository;
import com.wafa.api.repository.UtilisateurRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EnqueteService {

	@Autowired
	private EnqueteRepository repository;

	@Autowired
	private EnqueteMapper mapper;

	@Autowired
	private QuestionnaireMapper questmapper;

	@Autowired
	private UtilisateurService userService;

	@Autowired
	private EmailService emailService;
	
	@Autowired
	private UtilisateurRepository userRepository;

	@Autowired
	private ReponseEnqueteRepository reponseRepository;

	@Autowired
	private ReponseEnqueteMapper reponseEnqueteMapper;

	@Transactional(readOnly = true)
	public List<EnqueteDto> getAllEnquete() {

		log.info("L'affichage des Enquetes");
		List<Enquete> enquetes = repository.findAll();
		return mapper.mapToListOfDTO(enquetes);
	}

	@Transactional
	public void deleteEnquete(Long id) {
		if (id != null) {
			Enquete enqute = repository.findById(id).orElse(null);
			if (enqute == null) {
				throw new NotFoundException("l'enqute " + id + " est inexistant");
			}
			repository.deleteById(id);
		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la suppression d'une enqute");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la suppression d'une enqute");
		}
	}

	@Transactional(readOnly = true)
	public List<EnqueteDto> getByUser(Long id) {
		if (null != id) {

			log.info("L'affichage des idee a travers son user");
			Utilisateur user = userRepository.findById(id).orElse(null);
			if (user == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}
			List<Enquete> enquetes = repository.findByUtilisateurUtilisateurId(id);
			return mapper.mapToListOfDTO(enquetes);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des idee");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional
	public EnqueteDto saveEnquete(EnqueteDto enqueteDto) {
		if (enqueteDto != null) {
			log.info("La Creation d'un enquete ");

			userService.getUtilisateurById(enqueteDto.getUtilisateurId());

			Enquete enquete = mapper.mapToEntity(enqueteDto);

			Enquete savedEnquete = repository.save(enquete);
			
			String msg = "Bonjour,  \n \n";
			msg += "L'administrateur vient de publier une nouvelle enquête \n \n";
			msg += "Cordialement, \n";
			msg += "Equipe Innovation";
			emailService.broadcast(msg, "Nouvelle enquête");
			return mapper.mapToDTO(savedEnquete);

		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de la creation d'une enquete");
			throw new IllegalArgumentException(
					"Une Erreur Fonctionnelle s'est produite lors de la creation d'une enquete");
		}
	}

	@Transactional
	public EnqueteDto saveEnqueteWithQuestionnaire(EnqueteDto enqueteDto) {
		log.info("La Creation d'une Enquete ");
		if (enqueteDto != null) {
			userService.getUtilisateurById(enqueteDto.getUtilisateurId());
			List<Questionnaire> questionnaires = questmapper.mapToListOfEntity(enqueteDto.getQuestionnaires());

			Enquete enquete = mapper.mapToEntity(enqueteDto);

			for (Questionnaire questionnaire : questionnaires) {
				if (questionnaire != null) {
					questionnaire.setEnquete(enquete);
					List<ChoixReponse> choixReponses = questionnaire.getChoixReponses();
					if (choixReponses != null)
						for (ChoixReponse choixReponse : choixReponses) {
							if (choixReponse != null) {
								choixReponse.setQuestionnaire(questionnaire);
							}
						}
				}
			}

			enquete.setQuestionnaires(questionnaires);

			Enquete savedEnquete = repository.save(enquete);

			return mapper.mapToDTO(savedEnquete);

		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de la creation d'une enquete");
			throw new IllegalArgumentException(
					"Une Erreur Fonctionnelle s'est produite lors de la creation d'une enquete");
		}
	}

	@Transactional
	public EnqueteDto updateEnquete(EnqueteDto enqueteDto, Long id) {
		if (enqueteDto != null && id != null) {
			
			Enquete enquete = repository.findById(id).orElse(null);
			if (enquete == null) {
				throw new NotFoundException("l'enquete " + id + " Est inexistant");
			}
			userService.getUtilisateurById(enqueteDto.getUtilisateurId());
			enqueteDto.setEnqueteId(id);
			Enquete enqueteUpdate = mapper.mapToEntity(enqueteDto);
			Enquete savedEnquete = repository.saveAndFlush(enqueteUpdate);
			return mapper.mapToDTO(savedEnquete);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification d'un enquete");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la modification d'un enquete");
		}
	}

	@Transactional(readOnly = true)
	public EnqueteDto getEnqueteById(Long id) {
		if (null != id) {

			log.info("L'affichage d'une enquete a travers son Identifiant");
			Enquete enquete = repository.findById(id).orElse(null);
			if (enquete == null) {
				throw new NotFoundException("l'enquete " + id + " Est inexistant");
			}
			return mapper.mapToDTO(enquete);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage d'une enquete");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional(readOnly = true)
	public Page<EnqueteDto> getEnquetePagable(int page, int size) {
		List<Enquete> enquetes = repository.findAll();

		List<EnqueteDto> listEnqueteDto = mapper.mapToListOfDTO(enquetes);

		int start = (int) PageRequest.of(page, size).getOffset();
		int end = (start + PageRequest.of(page, size).getPageSize()) > listEnqueteDto.size() ? listEnqueteDto.size()
				: (start + PageRequest.of(page, size).getPageSize());

		return new PageImpl<>(listEnqueteDto.subList(start, end), PageRequest.of(page, size), listEnqueteDto.size());

	}

	@Transactional(readOnly = true)
	public EnqueteDto getEnqueteOfUser(Long idE, Long idUser) {

		if (idE != null || idUser != null) {
			EnqueteDto enqueteDto = getEnqueteById(idE);

			if (enqueteDto != null) {

				List<QuestionnaireDto> questionnaireDtos = enqueteDto.getQuestionnaires();

				if (questionnaireDtos != null && !questionnaireDtos.isEmpty()) {

					for (QuestionnaireDto questionnaireDto : questionnaireDtos) {

						ReponseEnquete reponses = reponseRepository
								.findByQuestionnaireQuestionnaireIdAndUtilisateurUtilisateurId(
										questionnaireDto.getQuestionnaireId(), idUser);
						ReponseEnqueteDto ReponseDto = reponseEnqueteMapper.mapToDTO(reponses);
						questionnaireDto.setReponse(ReponseDto);
					}
				}
				return enqueteDto;
			} else {
				throw new NotFoundException("l'enquete " + idE + " Est inexistant");
			}
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage d'une enquete");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}
}
