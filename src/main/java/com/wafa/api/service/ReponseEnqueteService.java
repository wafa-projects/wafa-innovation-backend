package com.wafa.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wafa.api.dto.ReponseEnqueteDto;
import com.wafa.api.entity.Enquete;
import com.wafa.api.entity.Questionnaire;
import com.wafa.api.entity.ReponseEnquete;
import com.wafa.api.entity.Utilisateur;
import com.wafa.api.exception.NotFoundException;
import com.wafa.api.exception.TechnicalException;
import com.wafa.api.mapper.ReponseEnqueteMapper;
import com.wafa.api.repository.EnqueteRepository;
import com.wafa.api.repository.QuestionnaireRepository;
import com.wafa.api.repository.ReponseEnqueteRepository;
import com.wafa.api.repository.UtilisateurRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ReponseEnqueteService {

	@Autowired
	private ReponseEnqueteRepository repository;

	@Autowired
	private QuestionnaireRepository questRepository;

	@Autowired
	private ReponseEnqueteMapper mapper;

	@Autowired
	private UtilisateurRepository userRepository;

	@Autowired
	private EnqueteRepository enqueteRepository;

	@Transactional(readOnly = true)
	public List<ReponseEnqueteDto> getAllReponseEnquete() {

		log.info("L'affichage des ReponseEnquetes");
		List<ReponseEnquete> reponseEnquetes = repository.findAll();
		return mapper.mapToListOfDTO(reponseEnquetes);
	}

	@Transactional
	public void deleteReponseEnquete(Long id) {
		if (id != null) {
			ReponseEnquete rEnquete = repository.findById(id).orElse(null);
			if (rEnquete == null) {
				throw new NotFoundException("rEnquete " + id + " est inexistant");
			}
			repository.deleteById(id);
		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la suppression d'une rEnquete");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la suppression d'une rEnquete");
		}
	}

	@Transactional
	public List<ReponseEnqueteDto> saveAllReponse(List<ReponseEnqueteDto> reponseDtos, Long idUser) {
		if (null != idUser && reponseDtos != null && !reponseDtos.isEmpty()) {

			log.info("La Creation des reponses ");
			Utilisateur user = userRepository.findById(idUser).orElse(null);
			if (user == null) {
				throw new NotFoundException("l'utilisateur " + idUser + " Est inexistant");
			}
			Questionnaire questionnaire = questRepository.findById(reponseDtos.get(0).getQuestionnaireId())
					.orElse(null);
			Enquete enquete = enqueteRepository.findById(questionnaire.getEnquete().getEnqueteId()).orElse(null);
			List<Utilisateur> utilisateurs = enquete.getParticipants();
			Boolean findUser=false;
			for(Utilisateur utilisateur:utilisateurs){
				if(utilisateur.getUtilisateurId().equals(idUser)){
					findUser=true;
				}
			}
			if(findUser){
				throw new TechnicalException("Vous avez déja particper a cette enquête");
			}
			utilisateurs.add(user);
			enquete.setParticipants(utilisateurs);
			enqueteRepository.saveAndFlush(enquete);
			
			for (ReponseEnqueteDto reponseDto : reponseDtos) {
				reponseDto.setUtilisateurId(idUser);
				ReponseEnquete reponse = mapper.mapToEntity(reponseDto);

				repository.save(reponse);

			}

			
			return reponseDtos;
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de la creation des reponse");
			throw new IllegalArgumentException(
					"Une Erreur Fonctionnelle s'est produite lors de la creation des reponse");
		}
	}

	@Transactional(readOnly = true)
	public List<ReponseEnqueteDto> getByUser(Long id) {
		if (null != id) {

			log.info("L'affichage des idee a travers son user");
			Utilisateur user = userRepository.findById(id).orElse(null);
			if (user == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}
			List<ReponseEnquete> enquetes = repository.findByUtilisateurUtilisateurId(id);
			return mapper.mapToListOfDTO(enquetes);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des idee");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	// @Transactional(readOnly = true)
	// public Boolean checkUserParticipate(Long id,Long idUser) {
	// if (null != id && idUser!=null) {
	//
	// log.info("L'affichage des idee a travers son user");
	// Utilisateur user = userRepository.findById(id).orElse(null);
	// if (user==null) {
	// throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
	// }
	// List<ReponseEnquete> enquetes =
	// repository.findByUtilisateurUtilisateurId(id);
	// return mapper.mapToListOfDTO(enquetes);
	// } else {
	// log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des
	// idee");
	// throw new IllegalArgumentException("Object Identifiant canot be null");
	// }
	// }

	@Transactional
	public ReponseEnqueteDto saveReponseEnquete(ReponseEnqueteDto reponseEnqueteDto) {
		if (reponseEnqueteDto != null) {
			log.info("La Creation d'un reponseEnquete ");

			ReponseEnquete reponseEnquete = mapper.mapToEntity(reponseEnqueteDto);

			ReponseEnquete savedReponseEnquete = repository.save(reponseEnquete);
			return mapper.mapToDTO(savedReponseEnquete);

		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de la creation d'une reponseEnquete");
			throw new IllegalArgumentException(
					"Une Erreur Fonctionnelle s'est produite lors de la creation d'une reponseEnquete");
		}
	}

	@Transactional
	public ReponseEnqueteDto updateReponseEnquete(ReponseEnqueteDto reponseEnqueteDto, Long id) {
		if (reponseEnqueteDto != null && id != null) {
			ReponseEnquete reponseEnquete = repository.findById(id).orElse(null);
			if (reponseEnquete == null) {
				throw new NotFoundException("l'reponseEnquete " + id + " Est inexistant");
			}

			reponseEnqueteDto.setReponseId(id);
			ReponseEnquete reponseEnqueteUpdate = mapper.mapToEntity(reponseEnqueteDto);
			ReponseEnquete savedReponseEnquete = repository.saveAndFlush(reponseEnqueteUpdate);
			return mapper.mapToDTO(savedReponseEnquete);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification d'un reponseEnquete");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la modification d'un reponseEnquete");
		}
	}

	@Transactional(readOnly = true)
	public ReponseEnqueteDto getReponseEnqueteById(Long id) {
		if (null != id) {

			log.info("L'affichage d'une reponseEnquete a travers son Identifiant");
			ReponseEnquete reponseEnquete = repository.findById(id).orElse(null);
			if (reponseEnquete == null) {
				throw new NotFoundException("l'reponseEnquete " + id + " Est inexistant");
			}
			return mapper.mapToDTO(reponseEnquete);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage d'une reponseEnquete");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional(readOnly = true)
	public Page<ReponseEnqueteDto> getReponseEnquetePagable(int page, int size) {
		List<ReponseEnquete> reponseEnquetes = repository.findAll();

		List<ReponseEnqueteDto> listReponseEnqueteDto = mapper.mapToListOfDTO(reponseEnquetes);

		int start = (int) PageRequest.of(page, size).getOffset();
		int end = (start + PageRequest.of(page, size).getPageSize()) > listReponseEnqueteDto.size()
				? listReponseEnqueteDto.size() : (start + PageRequest.of(page, size).getPageSize());

		return new PageImpl<>(listReponseEnqueteDto.subList(start, end), PageRequest.of(page, size),
				listReponseEnqueteDto.size());

	}

	@Transactional(readOnly = true)
	public Page<ReponseEnqueteDto> getReponseByQestionPagable(int page, int size, Long id) {
		if (null != id) {

			log.info("L'affichage des question a travers son parent");
			Questionnaire question = questRepository.findById(id).orElse(null);
			if (question == null) {
				throw new NotFoundException("l'utilisateur " + id + " Est inexistant");
			}
			List<ReponseEnquete> reponses = repository.findByQuestionnaireQuestionnaireId(id);
			List<ReponseEnqueteDto> listReponseDto = mapper.mapToListOfDTO(reponses);

			int start = (int) PageRequest.of(page, size).getOffset();
			int end = (start + PageRequest.of(page, size).getPageSize()) > listReponseDto.size() ? listReponseDto.size()
					: (start + PageRequest.of(page, size).getPageSize());

			return new PageImpl<>(listReponseDto.subList(start, end), PageRequest.of(page, size),
					listReponseDto.size());
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des question");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}

	}

}
