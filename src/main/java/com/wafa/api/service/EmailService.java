package com.wafa.api.service;


import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.wafa.api.entity.Utilisateur;
import com.wafa.api.repository.UtilisateurRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EmailService  {

	private JavaMailSender mailSender;
	
	@Autowired
	private UtilisateurRepository repository;
	
	@Value("${service.email.adress}")
	private String email;
	
	@Autowired
	public EmailService(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	public void sendSimpleMail(String to, String subject, String text) {

		MimeMessage message = mailSender.createMimeMessage();

		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
			helper.setFrom(email);
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setText(text);

			
			mailSender.send(message);
		}
		catch (MessagingException e) {
			e.printStackTrace();
			log.error(e.toString());
		}

	}
	
	public void broadcast(String msg,String subject){
		List<Utilisateur> users=repository.findByIsDeleted(false);
		for(Utilisateur user:users){
			sendSimpleMail(user.getEmail(), subject, msg);
		}
	}
	
	
}
