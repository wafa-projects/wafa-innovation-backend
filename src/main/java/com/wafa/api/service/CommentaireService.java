package com.wafa.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wafa.api.dto.CommentaireDto;
import com.wafa.api.entity.Article;
import com.wafa.api.entity.Commentaire;
import com.wafa.api.exception.NotFoundException;
import com.wafa.api.mapper.CommentaireMapper;
import com.wafa.api.repository.ArticleRepository;
import com.wafa.api.repository.CommentaireRepository;

import lombok.extern.slf4j.Slf4j;
import specification.CommentaireSpecifications;
import specification.filter.CommentaireFilter;

@Slf4j
@Service
public class CommentaireService {

	@Autowired
	private CommentaireRepository repository;

	@Autowired
	private ArticleRepository articleRepository;

	@Autowired
	private CommentaireMapper mapper;
	
	@Autowired
	private UtilisateurService userService;

	@Transactional
	public CommentaireDto saveCommentaire(CommentaireDto commentaireDto) {
		if (commentaireDto != null) {
			log.info("La Creation d'un commentaire ");
			userService.getUtilisateurById(commentaireDto.getUtilisateurId());

			Commentaire commentaire = mapper.mapToEntity(commentaireDto);

			Commentaire savedCommentaire = repository.save(commentaire);
			return mapper.mapToDTO(savedCommentaire);

		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de la creation d'une commentaire");
			throw new IllegalArgumentException(
					"Une Erreur Fonctionnelle s'est produite lors de la creation d'une commentaire");
		}
	}
	
	@Transactional(readOnly = true)
	public Page<CommentaireDto> getCommentaireByCriteria(CommentaireFilter commentaireFilter) {
		List<Commentaire> result = new ArrayList<>();
		log.info("Afficher la list des commentaire avec Pagination");

		Pageable pageable = PageRequest.of(commentaireFilter.getPage(), commentaireFilter.getSize(),
				 Sort.Direction.DESC, "dateCommentaire");
		
		Page<Commentaire> pageCommentaire = repository
				.findAll(Specification.where(CommentaireSpecifications.getByIDArticle(commentaireFilter.getArticleId()))
						.and(CommentaireSpecifications.getByDateDebut(commentaireFilter.getDateDebut()))
						.and(CommentaireSpecifications.getByDateFin(commentaireFilter.getDateFin()))
						.and(CommentaireSpecifications.getByDescription(commentaireFilter.getDescription()))
						.and(CommentaireSpecifications.getByVisble(commentaireFilter.getIsVisible()))
						.and(CommentaireSpecifications.getByNameUser(commentaireFilter.getNomUser()))
						.and(CommentaireSpecifications.getByID(commentaireFilter.getCommentaireId()))
						.and(CommentaireSpecifications.getByIDUser(commentaireFilter.getIdUser())), pageable);

						pageCommentaire.forEach(result::add);

		List<CommentaireDto> listCommentaireDto = mapper.mapToListOfDTO(result);
	
		return new PageImpl<>(listCommentaireDto, pageable, pageCommentaire.getTotalElements());
	}
	
	@Transactional
	public CommentaireDto validereCommentaire(Long id) {
		if (id != null) {
			Commentaire commentaire = repository.findById(id).orElse(null);
			if (commentaire == null) {
				throw new NotFoundException("Le commentaire " + id + " Est inexistant");
			}

			commentaire.setIsVisible(true);
			Commentaire savedCommentaire = repository.saveAndFlush(commentaire);
			return mapper.mapToDTO(savedCommentaire);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification d'un commentaire");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la modification d'un commentaire");
		}
	}
	@Transactional
	public void deleteCommentaire(Long id){
		if(id!=null){
			Commentaire commentaire = repository.findById(id).orElse(null);
			if (commentaire == null) {
				throw new NotFoundException("Le commentaire " + id + " est inexistant");
			}
			repository.deleteById(id);
		}
		else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la suppression d'un commentaire");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la suppression d'un commentaire");
		}
	}
	@Transactional
	public CommentaireDto updateCommentaire(CommentaireDto commentaireDto, Long id) {
		if (commentaireDto != null && id != null) {
			Commentaire commentaire = repository.findById(id).orElse(null);
			if (commentaire == null) {
				throw new NotFoundException("l'commentaire " + id + " Est inexistant");
			}
			userService.getUtilisateurById(commentaireDto.getUtilisateurId());
			commentaireDto.setCommentaireId(id);
			Commentaire commentaireUpdate = mapper.mapToEntity(commentaireDto);
			Commentaire savedCommentaire = repository.saveAndFlush(commentaireUpdate);
			return mapper.mapToDTO(savedCommentaire);

		} else {
			log.info("Une Erreur fonctionnelle s'est produite lors de la modification d'un commentaire");
			throw new IllegalArgumentException(
					"Une Erreur fonctionnelle s'est produite lors de la modification d'un commentaire");
		}
	}

	@Transactional(readOnly = true)
	public CommentaireDto getCommentaireById(Long id) {
		if (null != id) {

			log.info("L'affichage d'une commentaire a travers son Identifiant");
			Commentaire commentaire = repository.findById(id).orElse(null);
			if (commentaire == null) {
				throw new NotFoundException("l'commentaire " + id + " Est inexistant");
			}
			return mapper.mapToDTO(commentaire);
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage d'une commentaire");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}
	}

	@Transactional(readOnly = true)
	public Page<CommentaireDto> getByArticlePagable(int page, int size, Long id) {

		if (null != id) {

			log.info("L'affichage des question a travers son parent");
			Article article = articleRepository.findById(id).orElse(null);
			if (article == null) {
				throw new NotFoundException("l'article " + id + " Est inexistant");
			}
			List<Commentaire> commentaires = repository.findByArticleArticleId(id);
			List<CommentaireDto> listCommentaireDto = mapper.mapToListOfDTO(commentaires);

			int start = (int) PageRequest.of(page, size).getOffset();
			int end = (start + PageRequest.of(page, size).getPageSize()) > listCommentaireDto.size()
					? listCommentaireDto.size() : (start + PageRequest.of(page, size).getPageSize());

			return new PageImpl<>(listCommentaireDto.subList(start, end), PageRequest.of(page, size),
					listCommentaireDto.size());
		} else {
			log.info("Une Erreur Fonctionnelle s'est produite lors de l'affichage des question");
			throw new IllegalArgumentException("Object Identifiant canot be null");
		}

	}

}
