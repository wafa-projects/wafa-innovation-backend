package com.wafa.api.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReponseEnqueteDto {


	private Long reponseId;

//	private Long choixReponseId;
	
	private Long questionnaireId;
	
	private String reponse;
	
	private LocalDateTime dateReponse = LocalDateTime.now(); 

	
	private UtilisateurDto utilisateur;

	private Long utilisateurId;
}
