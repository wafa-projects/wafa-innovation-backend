package com.wafa.api.dto;

import java.time.LocalDateTime;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EnqueteDto {

	
	private Long enqueteId;

	private String description;
		
	private String titre;

	private List<QuestionnaireDto> questionnaires;
	
	private List<UtilisateurDto> participants;
	
	private UtilisateurDto utilisateur;
	
	private LocalDateTime dateEnquete = LocalDateTime.now();
	
	private Long utilisateurId;
}
