package com.wafa.api.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionDto {


	private Long questionId;

	private String description;
	
	private Boolean isVisible=false;
	
	private LocalDateTime dateQuestion = LocalDateTime.now(); 
	
	private LocalDateTime datePublication;

	private Long utilisateurId;
	
	private UtilisateurDto utilisateur;

	private Long questionParentId;
}
