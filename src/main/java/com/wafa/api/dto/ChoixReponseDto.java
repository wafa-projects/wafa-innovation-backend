package com.wafa.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChoixReponseDto {
	
	private Long choixId;

	private String description;

	private Long questionnaireID;
}
