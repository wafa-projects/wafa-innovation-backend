package com.wafa.api.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentaireDto {

	private Long commentaireId;

	private String description;
	
	private Boolean isVisible=false;

	private LocalDateTime dateCommentaire = LocalDateTime.now();

	private Long utilisateurId;
	
	private UtilisateurDto utilisateur;

	private Long articleId;
}
