package com.wafa.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthOpenIdDto {

	protected String realm;

	protected String client_id;

	protected String client_secret;

	protected String username;

	protected String password;

	protected String grant_type;

}
