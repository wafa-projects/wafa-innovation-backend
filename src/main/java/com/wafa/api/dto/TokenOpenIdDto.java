package com.wafa.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenOpenIdDto {

	private String access_token;
	private Integer expires_in;
	private Integer refresh_expires_in;
	private String refresh_token;
	private String token_type;
	private String scope;
	

	
}
