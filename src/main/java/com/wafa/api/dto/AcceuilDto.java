package com.wafa.api.dto;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import com.wafa.api.entity.PieceJointe;
import com.wafa.api.entity.Utilisateur;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AcceuilDto {

	private Long utilisateurId;
	
	private Long acceuilId;

	private String description;

	private String image;

	private UtilisateurDto utilisateur;

	private String phraseJours;

	private Boolean imageIsVisible=false;

}