package com.wafa.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

	private Long nbrQuestion;
	private Long nbrIdee;
	private Long nbrArticle;
	private Long nbrEnquete;

}
