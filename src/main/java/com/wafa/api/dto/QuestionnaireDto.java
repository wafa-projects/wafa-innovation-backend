package com.wafa.api.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionnaireDto {


	private Long questionnaireId;

	private String description;

	private Long enqueteId;
	
	private List<ChoixReponseDto> choixReponses;
	
	private ReponseEnqueteDto reponse;

}
