package com.wafa.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeycloakUser {

	private String id;

	private String firstName;

	private String lastName;

	private String username;

	private String email;
	
}
