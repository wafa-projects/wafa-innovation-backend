package com.wafa.api.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReponseDto {

	private Long reponseId;

	private String description;

	private LocalDateTime dateReponse = LocalDateTime.now();

	private Long utilisateurId;
	
	private UtilisateurDto utilisateur;

	private Long questionId;
}
