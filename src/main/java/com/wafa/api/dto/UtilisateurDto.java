package com.wafa.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UtilisateurDto {

	private Long utilisateurId;

	private String email;

	private String nom;

	private String prenom;

	private String password;

	private String telephone;

	private String matricule;

	private String entitee;

	private String ville;

	private String region;

	private String statut = "init";
	
	private Boolean isDeleted=false;

	private String role;
	
	private String photoProfile;

	private UserDto userDto;
	
	private TokenOpenIdDto tockenAuth;

}
