package com.wafa.api.dto;

import java.time.LocalDateTime;

import com.wafa.api.entity.PieceJointe;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BoiteIdeeDto {

	private Long ideeId;

	private String description;
	
	private Long utilisateurId;
	
	private String title;
	
	private LocalDateTime dateIdee = LocalDateTime.now();
	
	private UtilisateurDto utilisateur;

	private PieceJointe pieceJointe;
}
