package com.wafa.api.dto;

import java.time.LocalDateTime;

import com.wafa.api.entity.Utilisateur;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LikeDto {

	private Long likeId;

	private LocalDateTime dateLike = LocalDateTime.now();

	private Long utilisateurId;
	
	private UtilisateurDto utilisateur;
	
	private Long articleId;

}
