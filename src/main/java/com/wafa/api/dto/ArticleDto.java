package com.wafa.api.dto;

import java.time.LocalDateTime;

import com.wafa.api.entity.PieceJointe;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleDto {

	private Long articleId;

	private String description;
	
	private String article;
	
	private LocalDateTime dateArticle = LocalDateTime.now();
	
	private LocalDateTime datePublication;
	
	private Long utilisateurId;
	
	private Boolean isVisible=false;

	private PieceJointe pieceJointe;
	
	private UtilisateurDto utilisateur;
	
	private Long nbrLikes;
}
